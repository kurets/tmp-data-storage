package tmpstorage

import (
	"context"
	"errors"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

//DataToStoreDtoFactoryInterface is the interface for the factory
//which is responsible for creating new instance of DataToStoreDto
type DataToStoreDtoFactoryInterface interface {
	//Create is the method for DataToStoreDto creation
	//from parameters, which are needed in DataToStoreDto
	//(info regarding these parameters can be found	in DataToStoreDto docs)
	Create(dataKey string, data interface{}, expiresAt time.Time) DataToStoreDto

	//CreateWithExistenceDuration is the method for DataToStoreDto
	//creation from parameters:
	//
	//dataKey, data (info regarding these parameters can be found
	//in DataToStoreDto docs)
	//
	//existenceDuration (parameter, which indicates desired time
	//to live for stored data starting from the moment of
	//DataToStoreDto creation
	CreateWithExistenceDuration(dataKey string, data interface{}, existenceDuration time.Duration) DataToStoreDto
}

//DataToStoreDtoFactory is the factory for DataToStoreDto
//which has several ways of creating DataToStoreDto
type DataToStoreDtoFactory struct {
	createNowTimeService createNowTimeServiceInterface
}

func newDataToStoreDtoFactory(
	createNowTimeService createNowTimeServiceInterface,
) *DataToStoreDtoFactory {
	return &DataToStoreDtoFactory{
		createNowTimeService: createNowTimeService,
	}
}

//Create is the implementation of self-titled method, which is
//represented in DataToStoreDtoFactoryInterface
func (factory *DataToStoreDtoFactory) Create(
	dataKey string,
	data interface{},
	expiresAt time.Time,
) DataToStoreDto {
	return DataToStoreDto{
		DataKey:   dataKey,
		Data:      data,
		ExpiresAt: expiresAt,
	}
}

//CreateWithExistenceDuration is the implementation of self-titled method,
//which is represented in DataToStoreDtoFactoryInterface
func (factory *DataToStoreDtoFactory) CreateWithExistenceDuration(
	dataKey string,
	data interface{},
	existenceDuration time.Duration,
) DataToStoreDto {
	expiresAt := factory.createNowTimeService.execute().Add(existenceDuration)
	return factory.Create(dataKey, data, expiresAt)
}

type (
	dbConnector     func(ctx context.Context, opts ...*options.ClientOptions) (*mongo.Client, error)
	dbConnectorMode string
)

type dbConnectorFactory struct{}

func newDbConnectorFactory() *dbConnectorFactory {
	return new(dbConnectorFactory)
}

const mockDbConnectorMode dbConnectorMode = "mockDbConnectorMode"
const mockDbConnectorWithErrorMode dbConnectorMode = "mockDbConnectorWithErrorMode"

func (factory *dbConnectorFactory) create(connectorMode dbConnectorMode) dbConnector {
	switch connectorMode {
	case mockDbConnectorMode:
		return func(ctx context.Context, opts ...*options.ClientOptions) (*mongo.Client, error) {
			return new(mongo.Client), nil
		}
	case mockDbConnectorWithErrorMode:
		return func(ctx context.Context, opts ...*options.ClientOptions) (*mongo.Client, error) {
			return nil, errors.New("database connector error")
		}
	default:
		return mongo.Connect
	}
}

type createIndexIfNotExistsServiceFactoryInterface interface {
	create(parameterID *dataToStoreParameterID) (createIndexService createIndexIfNotExistsServiceInterface)
}

type createIndexIfNotExistsServiceFactory struct {
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface
	dbIndexManagerService                        dbIndexManagerServiceInterface
	dataKeyDbIndexIsDtoIsValidSpecification      dataKeyDbIndexIsDtoIsValidSpecificationInterface
	expiresAtDbIndexDtoIsValidSpecification      expiresAtDbIndexDtoIsValidSpecificationInterface
	logger                                       loggerServiceInterface
}

func newCreateIndexIfNotExistsServiceFactory(
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface,
	dbIndexManagerService dbIndexManagerServiceInterface,
	dataKeyDbIndexIsDtoIsValidSpecification dataKeyDbIndexIsDtoIsValidSpecificationInterface,
	expiresAtDbIndexDtoIsValidSpecification expiresAtDbIndexDtoIsValidSpecificationInterface,
	logger loggerServiceInterface,
) *createIndexIfNotExistsServiceFactory {
	return &createIndexIfNotExistsServiceFactory{
		getBsonParameterNameForDataToStoreDtoService: getBsonParameterNameForDataToStoreDtoService,
		dbIndexManagerService:                        dbIndexManagerService,
		dataKeyDbIndexIsDtoIsValidSpecification:      dataKeyDbIndexIsDtoIsValidSpecification,
		expiresAtDbIndexDtoIsValidSpecification:      expiresAtDbIndexDtoIsValidSpecification,
		logger:                                       logger,
	}
}

func (factory *createIndexIfNotExistsServiceFactory) create(
	parameterID *dataToStoreParameterID,
) (createIndexService createIndexIfNotExistsServiceInterface) {
	var err error
	switch parameterID.value {
	case dataKeyParameterID:
		createIndexService, err = newCreateUniqueIndexForDataKeyInCollectionIfNotExistsService(
			factory.getBsonParameterNameForDataToStoreDtoService,
			parameterID,
			factory.dbIndexManagerService,
			factory.dataKeyDbIndexIsDtoIsValidSpecification,
			factory.logger,
		)
	case expiresAtParameterID:
		createIndexService, err = newCreateTTLIndexForExpiresAtInCollectionIfNotExistsService(
			factory.getBsonParameterNameForDataToStoreDtoService,
			parameterID,
			factory.dbIndexManagerService,
			factory.expiresAtDbIndexDtoIsValidSpecification,
			factory.logger,
		)
	default:
		createIndexService = nil
		err = newParameterIDIsNotSupportedError(parameterID.value)
	}

	if err != nil {
		log.Panic(err)
	}

	return createIndexService
}
