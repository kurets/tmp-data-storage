package tmpstorage

import (
	"encoding/json"
	"go.mongodb.org/mongo-driver/bson"
	"io/ioutil"
	"log"
	"time"
)

type marshalerServiceInterface interface {
	marshalBSON(bsonMarshalData interface{}) ([]byte, error)
	marshalJSON(jsonMarshalData interface{}) ([]byte, error)
	unmarshalBSON(bsonEncodedData []byte, decodedResultPointer interface{}) error
	unmarshalJSON(jsonEncodedData []byte, decodedResultPointer interface{}) error
}

type marshalerService struct{}

func newMarshalerService() *marshalerService {
	return &marshalerService{}
}

func (service *marshalerService) marshalBSON(bsonMarshalData interface{}) ([]byte, error) {
	return bson.Marshal(bsonMarshalData)
}

func (service *marshalerService) marshalJSON(jsonMarshalData interface{}) ([]byte, error) {
	return json.Marshal(jsonMarshalData)
}

func (service *marshalerService) unmarshalBSON(bsonEncodedData []byte, decodedResultPointer interface{}) error {
	return bson.Unmarshal(bsonEncodedData, decodedResultPointer)
}

func (service *marshalerService) unmarshalJSON(jsonEncodedData []byte, decodedResultPointer interface{}) error {
	return json.Unmarshal(jsonEncodedData, decodedResultPointer)
}

type loggerServiceInterface interface {
	log(message string, level int)
	isDebug() isDebugMode
}

type isDebugMode bool

const (
	applicationNameLabel string = "tmpstorage"
	debugLevelLabel      string = "DEBUG"
	errorLevelLabel      string = "ERROR"
	debugLevel           int    = 1 << iota
	errorLevel
)

//logger is the services user for writing logs to
//stdout. It only writes logs to stdout if debug mode it ON
type loggerService struct {
	isDebugMode isDebugMode
}

//newLoggerService is the constructor of logger
func newLoggerService(isDebugModeOn isDebugMode) *loggerService {
	return &loggerService{isDebugMode: isDebugModeOn}
}

//log is the method for writing logs. It accepts message (string)
//and level (int). Message will be logged in format if log level
//in either "debugLevel":
//	(tmpstorage)[DEBUG]: message that need to be logged
//or "errorLevel":
//	(tmpstorage)[ERROR]: message that need to be logged
//or if level it invalid:
//	(tmpstorage): message that need to be logged
//
//If logger has been constructed with isDebugMode "false",
//all logs will be skipped
func (logger *loggerService) log(message string, level int) {
	if !logger.isDebug() {
		log.SetOutput(ioutil.Discard)
	}

	logFormat := "(%s)[%s]: %s"
	var label string
	switch level {
	case debugLevel:
		label = debugLevelLabel
	case errorLevel:
		label = errorLevelLabel
	default:
		logFormat = "(%s)%s: %s"
	}

	log.Printf(logFormat, applicationNameLabel, label, message)
}

func (logger *loggerService) isDebug() isDebugMode {
	return logger.isDebugMode
}

type createNowTimeServiceInterface interface {
	execute() time.Time
}

type createNowTimeService struct{}

func newCreateNowTimeService() *createNowTimeService {
	return new(createNowTimeService)
}

func (service *createNowTimeService) execute() time.Time {
	return time.Now()
}

type getBsonParameterNameForDataToStoreDtoServiceInterface interface {
	execute(parameterID *dataToStoreParameterID) (parameterName string)
}

type getBsonParameterNameForDataToStoreDtoService struct {
	dataToStoreDtoToBsonDMapper dataToStoreDtoToBsonDMapperInterface
}

func newGetBsonParameterNameForDataToStoreDtoService(
	dataToStoreDtoToBsonDMapper dataToStoreDtoToBsonDMapperInterface,
) *getBsonParameterNameForDataToStoreDtoService {
	return &getBsonParameterNameForDataToStoreDtoService{
		dataToStoreDtoToBsonDMapper: dataToStoreDtoToBsonDMapper,
	}
}

func (service *getBsonParameterNameForDataToStoreDtoService) execute(
	parameterID *dataToStoreParameterID,
) (
	parameterName string,
) {
	var dataToStoreBsonD bson.D
	err := service.dataToStoreDtoToBsonDMapper.executeMapping(DataToStoreDto{}, &dataToStoreBsonD)
	if err != nil {
		log.Panic(err)
	}

	return dataToStoreBsonD[parameterID.value].Key
}
