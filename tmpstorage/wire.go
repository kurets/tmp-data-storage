//+build wireinject

package tmpstorage

import (
	"github.com/google/wire"
)

func initializeDataStorageService(
	isDebugModeOn isDebugMode,
	database dbInterface,
) *DataStorage {
	wire.Build(
		newMarshalerService,
		wire.Bind(new(marshalerServiceInterface), new(*marshalerService)),
		newDataToStoreDtoToBsonDMapper,
		wire.Bind(new(dataToStoreDtoToBsonDMapperInterface), new(*dataToStoreDtoToBsonDMapper)),
		newGetBsonParameterNameForDataToStoreDtoService,
		wire.Bind(
			new(getBsonParameterNameForDataToStoreDtoServiceInterface),
			new(*getBsonParameterNameForDataToStoreDtoService),
		),
		newLoggerService,
		wire.Bind(new(loggerServiceInterface), new(*loggerService)),
		newGetCollectionForDataService,
		wire.Bind(new(getCollectionForDataServiceInterface), new(*getCollectionForDataService)),
		newDbIndexManagerService,
		wire.Bind(new(dbIndexManagerServiceInterface), new(*dbIndexManagerService)),
		newCreateIndexIfNotExistsServiceFactory,
		wire.Bind(new(createIndexIfNotExistsServiceFactoryInterface), new(*createIndexIfNotExistsServiceFactory)),
		newDataKeyDbIndexIsDtoIsValidSpecification,
		wire.Bind(new(dataKeyDbIndexIsDtoIsValidSpecificationInterface), new(*dataKeyDbIndexIsDtoIsValidSpecification)),
		newExpiresAtDbIndexDtoIsValidSpecification,
		wire.Bind(new(expiresAtDbIndexDtoIsValidSpecificationInterface), new(*expiresAtDbIndexDtoIsValidSpecification)),
		newAddDataToStorageService,
		wire.Bind(new(addDataToStorageServiceInterface), new(*addDataToStorageService)),
		newLoadDataFromStorageService,
		wire.Bind(new(loadDataFromStorageServiceInterface), new(*loadDataFromStorageService)),
		newDbFindOneResultToBsonDMapper,
		wire.Bind(new(dbFindOneResultToBsonDMapperInterface), new(*dbFindOneResultToBsonDMapper)),
		newDeleteDataFromStorageService,
		wire.Bind(new(deleteDataFromStorageServiceInterface), new(*deleteDataFromStorageService)),
		newUpdateDataInStorageService,
		wire.Bind(new(updateDataInStorageServiceInterface), new(*updateDataInStorageService)),
		newDataToStoreDtoFactory,
		wire.Bind(new(DataToStoreDtoFactoryInterface), new(*DataToStoreDtoFactory)),
		newCreateNowTimeService,
		wire.Bind(new(createNowTimeServiceInterface), new(*createNowTimeService)),
		newDataStorage,
	)

	return new(DataStorage)
}

func initializeConnectToMongoDbService(
	url dbURL,
	name dbName,
	databaseConnector dbConnector,
) *connectToMongoDbService {
	wire.Build(newConnectToMongoDbService)

	return new(connectToMongoDbService)
}

func initializeDbConnectorFactory() *dbConnectorFactory {
	wire.Build(newDbConnectorFactory)

	return new(dbConnectorFactory)
}
