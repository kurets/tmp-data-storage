package tmpstorage

import (
	"fmt"
	"log"
	"time"
)

func Example_dataStorageUsage() {
	storage, err := InitializeDataStorage(
		"mongodb://username:password@localhost:27017",
		"my_database",
		false,
	)

	if err != nil {
		log.Panic(err)
	}

	type Person struct {
		name string
		age  int
	}

	person := Person{name: "Mathew Murdock", age: 28}
	dataToStoreDto := storage.DataToStoreDtoFactory.CreateWithExistenceDuration(
		"thisIsMyDataKey",
		person,
		1000*time.Second, //TTL duration from now
	)

	err = storage.Add(dataToStoreDto)
	if err != nil {
		log.Panic(err)
	}

	err = storage.Update(dataToStoreDto)
	if err != nil {
		log.Panic(err)
	}

	loadedPerson := new(Person)
	//loadedPerson will be filled with data from storage
	err = storage.Load("thisIsMyDataKey", loadedPerson)
	if err != nil {
		log.Panic(err)
	}

	err = storage.Delete("thisIsMyDataKey", person)
	if err != nil {
		log.Panic(err)
	}
}

func Example_dataToStoreDtoFactory() {
	storage, err := InitializeDataStorage(
		"mongodb://username:password@localhost:27017",
		"my_database",
		false,
	)

	if err != nil {
		log.Panic(err)
	}

	type Person struct {
		name string
		age  int
	}

	person := Person{name: "Mathew Murdock", age: 28}

	//expiration time for data has been set to 120 seconds from now
	dataToStoreDtoExample1 := storage.DataToStoreDtoFactory.CreateWithExistenceDuration(
		"thisIsMyDataKeyOne",
		person,
		120*time.Second, //TTL duration from now
	)

	fmt.Println(dataToStoreDtoExample1)

	expirationTime, err := time.Parse("2006-01-02 15:04:05", "2025-01-01 00:00:03")
	if err != nil {
		log.Panic(err)
	}

	dataToStoreDtoExample2 := storage.DataToStoreDtoFactory.Create(
		"thisIsMyDataKeyTwo",
		person,
		expirationTime,
	)

	fmt.Println(dataToStoreDtoExample2)
}
