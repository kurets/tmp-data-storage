package tmpstorage

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
	"testing"
)

type dbFindOneResultToBsonDMapperTest struct {
	dbFindOneResultToBsonDMapper dbFindOneResultToBsonDMapperInterface
	dataForTest                  struct {
		mappedBsonD   *bson.D
		expectedError error
	}
	mocks struct {
		dbFindOneResult *mockDbFindOneResultInterface
	}
}

func TestDbFindOneResultToBsonDMapperTest(t *testing.T) {
	mapperTest := new(dbFindOneResultToBsonDMapperTest)
	mapperTest.dbFindOneResultToBsonDMapper = newDbFindOneResultToBsonDMapper()
	mapperTest.prepareDataForTest()
	mapperTest.prepareMocks()

	t.Parallel()
	t.Run("testSuccessfulMapping", mapperTest.testSuccessfulMapping)
	t.Run("testFailureOnDbFindOneResultDecodingError", mapperTest.testFailureOnDbFindOneResultDecodingError)
}

func (test *dbFindOneResultToBsonDMapperTest) testSuccessfulMapping(t *testing.T) {
	bsonForMapping := new(bson.D)
	test.mocks.dbFindOneResult.
		On("Decode", bsonForMapping).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(0).(*bson.D)
			*arg = *test.dataForTest.mappedBsonD
		})

	err := test.dbFindOneResultToBsonDMapper.executeMapping(
		test.mocks.dbFindOneResult,
		bsonForMapping,
	)

	assert.Nil(t, err)
	assert.Equal(t, test.dataForTest.mappedBsonD, bsonForMapping)
}

func (test *dbFindOneResultToBsonDMapperTest) testFailureOnDbFindOneResultDecodingError(t *testing.T) {
	bsonForMapping := new(bson.D)
	test.mocks.dbFindOneResult.
		On("Decode", bsonForMapping).
		Return(test.dataForTest.expectedError).
		Once()

	err := test.dbFindOneResultToBsonDMapper.executeMapping(test.mocks.dbFindOneResult, bsonForMapping)

	assert.Error(t, err)
	assert.EqualError(t, err, test.dataForTest.expectedError.Error())
}

func (test *dbFindOneResultToBsonDMapperTest) prepareDataForTest() {
	test.dataForTest.mappedBsonD = &bson.D{{Key: "someKey", Value: "data for some key"}}
	test.dataForTest.expectedError = errors.New("some error")
}

func (test *dbFindOneResultToBsonDMapperTest) prepareMocks() {
	test.mocks.dbFindOneResult = new(mockDbFindOneResultInterface)
}

type dataToStoreDtoToBsonDMapperTest struct {
	dataToStoreDtoToBsonDMapper dataToStoreDtoToBsonDMapperInterface
	dataForTest                 struct {
		validDataToStoreDto DataToStoreDto
		bsonBytes           []byte
		inputBsonD          *bson.D
		expectedError       *failedToMapDataToStoreDtoToBson
		expectedBsonD       *bson.D
	}
	marshalerService *mockMarshalerServiceInterface
}

func TestDataToStoreDtoToBsonDMapper(t *testing.T) {
	mapperTest := dataToStoreDtoToBsonDMapperTest{}
	mapperTest.prepareDataForTest()
	mapperTest.marshalerService = new(mockMarshalerServiceInterface)
	mapperTest.dataToStoreDtoToBsonDMapper = newDataToStoreDtoToBsonDMapper(mapperTest.marshalerService)

	t.Parallel()
	t.Run("testSuccessfulMapping", mapperTest.testSuccessfulMapping)
	t.Run("testFailureOnMarshalBSON", mapperTest.testFailureOnMarshalBSON)
	t.Run("testFailureOnUnmarshalBSON", mapperTest.testFailureOnUnmarshalBSON)
}

func (test *dataToStoreDtoToBsonDMapperTest) testSuccessfulMapping(t *testing.T) {
	test.marshalerService.
		On("marshalBSON", test.dataForTest.validDataToStoreDto).
		Return(test.dataForTest.bsonBytes, nil).
		Once()

	test.marshalerService.
		On("unmarshalBSON", test.dataForTest.bsonBytes, test.dataForTest.inputBsonD).
		Return(nil).
		Once()

	err := test.dataToStoreDtoToBsonDMapper.executeMapping(
		test.dataForTest.validDataToStoreDto,
		test.dataForTest.inputBsonD,
	)

	assert.Nil(t, err)
}

func (test *dataToStoreDtoToBsonDMapperTest) testFailureOnMarshalBSON(t *testing.T) {
	test.marshalerService.
		On("marshalBSON", test.dataForTest.validDataToStoreDto).
		Return([]byte{}, errors.New("marshaler error")).
		Once()

	err := test.dataToStoreDtoToBsonDMapper.executeMapping(
		test.dataForTest.validDataToStoreDto,
		test.dataForTest.inputBsonD,
	)

	assert.Error(t, err)
	assert.EqualError(t, err, test.dataForTest.expectedError.message)
	assert.Error(t, err.Previous())
}

func (test *dataToStoreDtoToBsonDMapperTest) testFailureOnUnmarshalBSON(t *testing.T) {
	test.marshalerService.
		On("marshalBSON", test.dataForTest.validDataToStoreDto).
		Return(test.dataForTest.bsonBytes, nil).
		Once()

	test.marshalerService.
		On("unmarshalBSON", test.dataForTest.bsonBytes, test.dataForTest.inputBsonD).
		Return(errors.New("marshaler error")).
		Once()

	err := test.dataToStoreDtoToBsonDMapper.executeMapping(
		test.dataForTest.validDataToStoreDto,
		test.dataForTest.inputBsonD,
	)

	assert.Error(t, err)
	assert.EqualError(t, err, test.dataForTest.expectedError.message)
	assert.Error(t, err.Previous())
}

func (test *dataToStoreDtoToBsonDMapperTest) prepareDataForTest() {
	test.dataForTest.validDataToStoreDto = DataToStoreDto{}
	test.dataForTest.expectedError = newFailedToMapDataToStoreDtoToBson(nil)
	test.dataForTest.bsonBytes = []byte{uint8(44), uint8(0), uint8(0), uint8(2), uint8(100), uint8(97), uint8(116)}
}
