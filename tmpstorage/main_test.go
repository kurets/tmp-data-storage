package tmpstorage

import (
	"log"
	"os"
	"os/exec"
	"testing"
)

//go:generate mockery --all --testonly --inpackage
func TestMain(m *testing.M) {
	setDbConnectorToMockMode()
	exitCode := m.Run()
	_ = exec.Command("rm", "mock_*").Run()
	os.Exit(exitCode)
}

func setDbConnectorToMockMode() {
	err := os.Setenv("DB_CONNECTOR_MODE", string(mockDbConnectorMode))
	if err != nil {
		log.Panic(err)
	}
}

func setDbConnectorToMockWithErrorMode() {
	err := os.Setenv("DB_CONNECTOR_MODE", string(mockDbConnectorWithErrorMode))
	if err != nil {
		log.Panic(err)
	}
}
