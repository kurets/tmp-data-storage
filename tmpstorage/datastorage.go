package tmpstorage

import (
	"context"
	"errors"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"os"
)

//DataStorage is the main set of the package, which is used
//to store, load, delete data to/from temporary data storage
type DataStorage struct {
	DataToStoreDtoFactory        DataToStoreDtoFactoryInterface
	addDataToStorageService      addDataToStorageServiceInterface
	loadDataFromStorageService   loadDataFromStorageServiceInterface
	deleteDataFromStorageService deleteDataFromStorageServiceInterface
	updateDataInStorageService   updateDataInStorageServiceInterface
	logger                       loggerServiceInterface
}

func newDataStorage(
	DataToStoreDtoFactory DataToStoreDtoFactoryInterface,
	addDataToStorageService addDataToStorageServiceInterface,
	loadDataFromStorageService loadDataFromStorageServiceInterface,
	deleteDataFromStorageService deleteDataFromStorageServiceInterface,
	updateDataInStorageService updateDataInStorageServiceInterface,
	logger loggerServiceInterface,
) *DataStorage {
	return &DataStorage{
		DataToStoreDtoFactory:        DataToStoreDtoFactory,
		addDataToStorageService:      addDataToStorageService,
		loadDataFromStorageService:   loadDataFromStorageService,
		deleteDataFromStorageService: deleteDataFromStorageService,
		updateDataInStorageService:   updateDataInStorageService,
		logger:                       logger,
	}
}

//InitializeDataStorage is used to initialize DataStorage
//which requires mongo database URL and the name of the database
//for storing data. `isDebugModeOn` is responsible for turning on/off
//logs, provided by tmpstorage package
func InitializeDataStorage(databaseURL, databaseName string, isDebugModeOn bool) (*DataStorage, error) {
	dbConnectorFactory := initializeDbConnectorFactory()
	dbConnectorMode := dbConnectorMode(os.Getenv("DB_CONNECTOR_MODE"))
	dbConnector := dbConnectorFactory.create(dbConnectorMode)

	connectToMongoDbService := initializeConnectToMongoDbService(
		dbURL(databaseURL),
		dbName(databaseName),
		dbConnector,
	)
	mongoDbConnection, err := connectToMongoDbService.connect()
	if err != nil {
		return nil, err
	}

	database := connectToMongoDbService.database(mongoDbConnection)

	dataStorageService := initializeDataStorageService(isDebugMode(isDebugModeOn), database)
	return dataStorageService, nil
}

//Add method is responsible for storing data. It accepts DataToStoreDto
//as input parameter. Data is stored in the collection with the name,
//based on DataToStoreDto.Data type
func (storage *DataStorage) Add(dataToStoreDto DataToStoreDto) TraceableErrorInterface {
	return storage.addDataToStorageService.executeAdding(dataToStoreDto)
}

//Load method is responsible for loading data from the storage. It looks
//for data by parameter `dataKey` in the collection with the name,
//based on dataPointerMapped type. `dataPointerMapped` is the pointer to empty
//instance of previously stored data. In case data is found in storage,
//`dataPointerMapped` will be filled with information from the storage.
//In case nothing is found, TraceableErrorInterface will be returned
func (storage *DataStorage) Load(dataKey string, dataPointer interface{}) TraceableErrorInterface {
	return storage.loadDataFromStorageService.executeLoading(dataKey, dataPointer)
}

//Delete method is responsible for removing data from storage. It looks
//for data by parameter `dataKey` and all the parameters from `data` in
//the collection with name, based on type of `data`. Same data can be
//passed, which has been passed to `Load` method.
//In case nothing is deleted after method calling, TraceableErrorInterface will be returned
func (storage *DataStorage) Delete(dataKey string, data interface{}) TraceableErrorInterface {
	return storage.deleteDataFromStorageService.executeDeletion(dataKey, data)
}

//Update method is responsible for updating data in storage. It accepts
//DataToStoreDto as input parameter. DataToStoreDto.DataKey is used for
//search, DataToStoreDto.Data and DataToStoreDto.ExpiresAt are used for
//updating and will replace currently storage values.
//In case nothing is updated, TraceableErrorInterface will be returned
func (storage *DataStorage) Update(dataToStoreDto DataToStoreDto) TraceableErrorInterface {
	return storage.updateDataInStorageService.executeUpdating(dataToStoreDto)
}

//AddOrUpdate method is simply a usage of Add and Update methods. If
//there is no data with such key in storage, method Add will be used,
//otherwise Update will be used
func (storage *DataStorage) AddOrUpdate(dataToStoreDto DataToStoreDto) TraceableErrorInterface {
	storage.logger.log("Trying to use 'Add' method on dataToStoreDto", debugLevel)
	err := storage.Add(dataToStoreDto)
	if err == nil {
		return nil
	}

	storage.logger.log("Trying to update dataToStoreDto after 'Add' method failure", debugLevel)

	err = storage.Update(dataToStoreDto)
	return err
}

type addDataToStorageServiceInterface interface {
	executeAdding(dataToStoreDto DataToStoreDto) TraceableErrorInterface
}

type addDataToStorageService struct {
	getCollectionForDataService          getCollectionForDataServiceInterface
	createIndexIfNotExistsServiceFactory createIndexIfNotExistsServiceFactoryInterface
	logger                               loggerServiceInterface
}

func newAddDataToStorageService(
	getCollectionForDataService getCollectionForDataServiceInterface,
	createIndexIfNotExistsServiceFactory createIndexIfNotExistsServiceFactoryInterface,
	logger loggerServiceInterface,
) *addDataToStorageService {
	return &addDataToStorageService{
		getCollectionForDataService:          getCollectionForDataService,
		createIndexIfNotExistsServiceFactory: createIndexIfNotExistsServiceFactory,
		logger:                               logger,
	}
}

func (service *addDataToStorageService) executeAdding(dataToStoreDto DataToStoreDto) TraceableErrorInterface {
	collection := service.getCollectionForDataService.execute(dataToStoreDto.Data)
	expiresAtParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID)
	createTTLIndexService := service.createIndexIfNotExistsServiceFactory.create(expiresAtParameterIDValueObject)
	createTTLIndexService.execute(collection)

	dataKeyParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	createUniqueIndexService := service.createIndexIfNotExistsServiceFactory.create(dataKeyParameterIDValueObject)
	createUniqueIndexService.execute(collection)

	_, err := collection.InsertOne(context.TODO(), dataToStoreDto)
	if err != nil {
		service.logger.log(
			fmt.Sprintf("Failed to add data with key '%s' to storage due to an error '%s'",
				dataToStoreDto.DataKey,
				err,
			),
			errorLevel,
		)

		return newFailedToAddDataToStorageError(err)
	}

	service.logger.log(
		fmt.Sprintf("Data with key '%s' has been added to storage", dataToStoreDto.DataKey),
		debugLevel,
	)

	return nil
}

type loadDataFromStorageServiceInterface interface {
	executeLoading(dataKey string, dataPointer interface{}) TraceableErrorInterface
}

type loadDataFromStorageService struct {
	getCollectionForDataService                  getCollectionForDataServiceInterface
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface
	marshalerService                             marshalerServiceInterface
	dbFindOneResultToBsonDMapper                 dbFindOneResultToBsonDMapperInterface
	logger                                       loggerServiceInterface
}

func newLoadDataFromStorageService(
	getCollectionForDataService getCollectionForDataServiceInterface,
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface,
	marshalerService marshalerServiceInterface,
	dbFindOneResultToBsonDMapper dbFindOneResultToBsonDMapperInterface,
	logger loggerServiceInterface,
) *loadDataFromStorageService {
	return &loadDataFromStorageService{
		getCollectionForDataService:                  getCollectionForDataService,
		getBsonParameterNameForDataToStoreDtoService: getBsonParameterNameForDataToStoreDtoService,
		marshalerService:                             marshalerService,
		dbFindOneResultToBsonDMapper:                 dbFindOneResultToBsonDMapper,
		logger:                                       logger,
	}
}

func (service *loadDataFromStorageService) executeLoading(
	dataKey string,
	dataPointer interface{},
) TraceableErrorInterface {
	collection := service.getCollectionForDataService.execute(dataPointer)

	dataKeyParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	dataKeyDbFieldName := service.getBsonParameterNameForDataToStoreDtoService.execute(dataKeyParameterIDValueObject)

	filter := bson.M{dataKeyDbFieldName: dataKey}
	result := collection.FindOne(context.TODO(), filter)

	var resultBsonD bson.D
	err := service.dbFindOneResultToBsonDMapper.executeMapping(result, &resultBsonD)
	if err != nil {
		service.logger.log(
			fmt.Sprintf("Failed to load dataPointerMapped from storage due to an error: %s", err),
			errorLevel,
		)

		return newFailedToLoadDataFromStorageError(err)
	}

	rawBsonDataFromResult, err := service.marshalerService.marshalBSON(resultBsonD[2].Value)
	if err != nil {
		log.Panic(err)
	}

	dataFromResultBsonM := new(bson.M)
	err = service.marshalerService.unmarshalBSON(rawBsonDataFromResult, dataFromResultBsonM)
	if err != nil {
		log.Panic(err)
	}

	dataFromResultJSONBytes, err := service.marshalerService.marshalJSON(dataFromResultBsonM)
	if err != nil {
		log.Panic(err)
	}

	err = service.marshalerService.unmarshalJSON(dataFromResultJSONBytes, dataPointer)
	if err != nil {
		log.Panic(err)
	}

	service.logger.log("Data has been successfully loaded from storage", debugLevel)
	return nil
}

type deleteDataFromStorageServiceInterface interface {
	executeDeletion(dataKey string, dataToDelete interface{}) TraceableErrorInterface
}

type deleteDataFromStorageService struct {
	getCollectionForDataService                  getCollectionForDataServiceInterface
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface
	logger                                       loggerServiceInterface
}

func newDeleteDataFromStorageService(
	getCollectionForDataService getCollectionForDataServiceInterface,
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface,
	logger loggerServiceInterface,
) *deleteDataFromStorageService {
	return &deleteDataFromStorageService{
		getCollectionForDataService:                  getCollectionForDataService,
		getBsonParameterNameForDataToStoreDtoService: getBsonParameterNameForDataToStoreDtoService,
		logger: logger,
	}
}

func (service *deleteDataFromStorageService) executeDeletion(
	dataKey string,
	dataToDelete interface{},
) TraceableErrorInterface {
	collection := service.getCollectionForDataService.execute(dataToDelete)

	dataKeyParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	dataKeyDbField := service.getBsonParameterNameForDataToStoreDtoService.execute(dataKeyParameterIDValueObject)

	dataParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(dataParameterID)
	dataDbField := service.getBsonParameterNameForDataToStoreDtoService.execute(dataParameterIDValueObject)

	result, err := collection.DeleteOne(
		context.TODO(), bson.M{
			dataKeyDbField: dataKey,
			dataDbField:    dataToDelete,
		},
	)

	if err != nil {
		service.logger.log(
			fmt.Sprintf("Failed to delete data from storage due to an error: %s", err),
			errorLevel,
		)
		return newFailedToDeleteDataFromStorageError(err)
	}

	if 0 == result.DeletedCount {
		err = errors.New("nothing has been deleted")
		service.logger.log(fmt.Sprintf("Failed to delete data from storage due to an error: %s", err), errorLevel)

		return newFailedToDeleteDataFromStorageError(err)
	}

	service.logger.log("Data has been successfully deleted from storage", debugLevel)
	return nil
}

type updateDataInStorageServiceInterface interface {
	executeUpdating(dataToStoreDto DataToStoreDto) TraceableErrorInterface
}

type updateDataInStorageService struct {
	getCollectionForDataService                  getCollectionForDataServiceInterface
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface
	logger                                       loggerServiceInterface
}

func newUpdateDataInStorageService(
	getCollectionForDataService getCollectionForDataServiceInterface,
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface,
	logger loggerServiceInterface,
) *updateDataInStorageService {
	return &updateDataInStorageService{
		getCollectionForDataService:                  getCollectionForDataService,
		getBsonParameterNameForDataToStoreDtoService: getBsonParameterNameForDataToStoreDtoService,
		logger: logger,
	}
}

func (service *updateDataInStorageService) executeUpdating(dataToStoreDto DataToStoreDto) TraceableErrorInterface {
	collection := service.getCollectionForDataService.execute(dataToStoreDto.Data)

	dataKeyParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	dataKeyDbField := service.getBsonParameterNameForDataToStoreDtoService.execute(dataKeyParameterIDValueObject)

	dataParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(dataParameterID)
	dataDbField := service.getBsonParameterNameForDataToStoreDtoService.execute(dataParameterIDValueObject)

	expiresAtParameterIDValueObject := newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID)
	expiresAtDbField := service.getBsonParameterNameForDataToStoreDtoService.execute(
		expiresAtParameterIDValueObject,
	)

	result, err := collection.UpdateOne(
		context.TODO(),
		bson.M{dataKeyDbField: dataToStoreDto.DataKey},
		bson.M{
			"$set": bson.M{
				dataDbField:      dataToStoreDto.Data,
				expiresAtDbField: dataToStoreDto.ExpiresAt,
			},
		},
	)

	if err != nil {
		service.logger.log(fmt.Sprintf("Failed to update data in storage due to an error: %s", err), errorLevel)
		return newFailedToUpdateDataInStorageError(err)
	}

	if 0 == result.ModifiedCount {
		err := errors.New("nothing has been updated")
		service.logger.log(
			fmt.Sprintf("Failed to update data in storage due to an error: %s", err),
			errorLevel,
		)
		return newFailedToUpdateDataInStorageError(err)
	}

	service.logger.log("Data in storage has been successfully updated", debugLevel)
	return nil
}
