package tmpstorage

import "time"

//DataToStoreDto is the structure, which is used by DataStorage
//as the input parameter when storing to storage or deleting from it
//
// DataKey is the identified for stored data
// e.g. ULID or UUID can be used. Value should be unique within collection
// nonetheless same value can be used for different types of stored data
//
// Data is the structure, which must be stored. It will be stored in
// self-named collection
//
// ExpiresAt is the time.Time structure, which indicates time after
// which data will be removed from storage automatically
type DataToStoreDto struct {
	DataKey   string      `bson:"dataKey"`
	Data      interface{} `bson:"data"`
	ExpiresAt time.Time   `bson:"expiresAt"`
}

//dataKeyDbIndexDto is the dto for dataKey index in db
//which is used for expiring stored data
type dataKeyDbIndexDto struct {
	Version int  `bson:"v"`
	Unique  bool `bson:"unique"`
	Key     struct {
		DataKey int
	} `bson:"key"`
	Name      string `bson:"name"`
	Namespace string `bson:"ns"`
}

//expiresAtDbIndexDto is the dto for expiresAt index in db
//which is used for expiring stored data
type expiresAtDbIndexDto struct {
	Version int `bson:"v"`
	Key     struct {
		ExpiresAt int
	} `bson:"key"`
	Name               string `bson:"name"`
	Namespace          string `bson:"ns"`
	ExpireAfterSeconds int    `bson:"expireAfterSeconds"`
}
