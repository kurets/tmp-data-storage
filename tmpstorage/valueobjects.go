package tmpstorage

import "log"

type dataToStoreParameterID struct {
	value int
}

const (
	dataKeyParameterID int = iota
	dataParameterID
	expiresAtParameterID
)

func newDataToStoreParameterID(value int) (*dataToStoreParameterID, error) {
	valueObject := new(dataToStoreParameterID)
	switch value {
	case dataKeyParameterID:
		fallthrough
	case dataParameterID:
		fallthrough
	case expiresAtParameterID:
		valueObject.value = value
		return valueObject, nil
	default:
		return nil, newInvalidDataToStoreParameterID(value)
	}
}

func newDataToStoreParameterIDWithPanicOnError(value int) *dataToStoreParameterID {
	valueObject, err := newDataToStoreParameterID(value)
	if err != nil {
		log.Panic(err)
	}

	return valueObject
}
