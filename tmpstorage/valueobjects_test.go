package tmpstorage

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"testing"
)

type dataToStoreParameterIDTest struct {
	dataForTest struct {
		correctValueSlice       []int
		incorrectValueDataSlice []struct {
			value         int
			expectedError error
		}
	}
}

func TestDataToStoreParameterID(t *testing.T) {
	test := dataToStoreParameterIDTest{}
	test.prepareDataForTest()

	t.Parallel()
	t.Run(
		"testSuccessfulConstructionViaNewDataToStoreParameterID",
		test.testSuccessfulConstructionViaNewDataToStoreParameterID,
	)
	t.Run(
		"testErrorReturnConstructionViaNewDataToStoreParameterID",
		test.testErrorReturnConstructionViaNewDataToStoreParameterID,
	)
	t.Run(
		"testSuccessfulConstructionViaNewDataToStoreParameterIDWithPanicOnError",
		test.testSuccessfulConstructionViaNewDataToStoreParameterIDWithPanicOnError,
	)
	t.Run(
		"testPanicsOnErrorWhileConstructionViaNewDataToStoreParameterIDWithPanicOnError",
		test.testPanicsOnErrorWhileConstructionViaNewDataToStoreParameterIDWithPanicOnError,
	)
}

func (test *dataToStoreParameterIDTest) testSuccessfulConstructionViaNewDataToStoreParameterID(t *testing.T) {
	for _, value := range test.dataForTest.correctValueSlice {
		parameterID, err := newDataToStoreParameterID(value)
		assert.IsType(t, &dataToStoreParameterID{}, parameterID)
		assert.Nil(t, err)
	}
}

func (test *dataToStoreParameterIDTest) testErrorReturnConstructionViaNewDataToStoreParameterID(t *testing.T) {
	for _, data := range test.dataForTest.incorrectValueDataSlice {
		parameterID, err := newDataToStoreParameterID(data.value)
		assert.Nil(t, parameterID)
		assert.Errorf(t, err, data.expectedError.Error())
	}
}

func (test *dataToStoreParameterIDTest) testSuccessfulConstructionViaNewDataToStoreParameterIDWithPanicOnError(
	t *testing.T,
) {
	for _, value := range test.dataForTest.correctValueSlice {
		parameterID := newDataToStoreParameterIDWithPanicOnError(value)
		assert.IsType(t, &dataToStoreParameterID{}, parameterID)
	}
}

func (test *dataToStoreParameterIDTest) testPanicsOnErrorWhileConstructionViaNewDataToStoreParameterIDWithPanicOnError(
	t *testing.T,
) {
	for _, data := range test.dataForTest.incorrectValueDataSlice {
		assert.PanicsWithValue(
			t,
			fmt.Sprint(data.expectedError.Error()),
			func() {
				newDataToStoreParameterIDWithPanicOnError(data.value)
			},
		)
	}
}

func (test *dataToStoreParameterIDTest) prepareDataForTest() {
	test.dataForTest.correctValueSlice = []int{0, 1, 2}
	test.dataForTest.incorrectValueDataSlice = []struct {
		value         int
		expectedError error
	}{
		{-100, newInvalidDataToStoreParameterID(-100)},
		{-1, newInvalidDataToStoreParameterID(-1)},
		{3, newInvalidDataToStoreParameterID(3)},
		{1234, newInvalidDataToStoreParameterID(1234)},
	}
}
