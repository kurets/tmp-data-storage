package tmpstorage

import (
	"bytes"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
	"log"
	"os"
	"testing"
	"time"
)

type marshalerServiceTest struct {
	marshalerService marshalerServiceInterface
	dataForTest      struct {
		testStruct      testStructForMarshalerService
		bsonEncodedData []byte
		jsonEncodedData []byte
	}
}

type testStructForMarshalerService struct {
	TestName        string `json:"testName" bson:"testName"`
	TestAge         int    `json:"testAge" bson:"testAge"`
	TestOtherString string
}

func TestMarshalerService(t *testing.T) {
	test := marshalerServiceTest{}
	test.prepareDataForTest()
	test.marshalerService = newMarshalerService()

	t.Parallel()
	t.Run("testMarshalBSON", test.testMarshalBSON)
	t.Run("testMarshalJSON", test.testMarshalJSON)
	t.Run("testUnmarshalBSON", test.testUnmarshalBSON)
	t.Run("testUnmarshalJSON", test.testUnmarshalJSON)
}

func (test *marshalerServiceTest) testMarshalBSON(t *testing.T) {
	actualEncodedData, actualErr := test.marshalerService.marshalBSON(test.dataForTest.testStruct)
	assert.Nil(t, actualErr)
	assert.Equal(t, test.dataForTest.bsonEncodedData, actualEncodedData)
}

func (test *marshalerServiceTest) testMarshalJSON(t *testing.T) {
	actualEncodedData, actualErr := test.marshalerService.marshalJSON(test.dataForTest.testStruct)
	assert.Nil(t, actualErr)
	assert.Equal(t, test.dataForTest.jsonEncodedData, actualEncodedData)
}

func (test *marshalerServiceTest) testUnmarshalBSON(t *testing.T) {
	structToFill := new(testStructForMarshalerService)
	actualErr := test.marshalerService.unmarshalBSON(test.dataForTest.bsonEncodedData, structToFill)
	assert.Nil(t, actualErr)
	assert.Equal(t, test.dataForTest.testStruct, *structToFill)
}

func (test *marshalerServiceTest) testUnmarshalJSON(t *testing.T) {
	structToFill := new(testStructForMarshalerService)
	actualErr := test.marshalerService.unmarshalJSON(test.dataForTest.jsonEncodedData, structToFill)
	assert.Nil(t, actualErr)
	assert.Equal(t, test.dataForTest.testStruct, *structToFill)
}

func (test *marshalerServiceTest) prepareDataForTest() {
	test.dataForTest.bsonEncodedData = []byte{
		87, 0, 0, 0, 2, 116, 101, 115, 116, 78, 97, 109, 101, 0, 16, 0, 0, 0, 77, 97, 116, 116, 104, 101, 119, 32,
		77, 117, 114, 100, 111, 99, 107, 0, 16, 116, 101, 115, 116, 65, 103, 101, 0, 28, 0, 0, 0, 2, 116, 101, 115,
		116, 111, 116, 104, 101, 114, 115, 116, 114, 105, 110, 103, 0, 18, 0, 0, 0, 115, 111, 109, 101, 32, 79, 84,
		72, 69, 82, 32, 115, 116, 114, 105, 110, 103, 0, 0,
	}
	test.dataForTest.jsonEncodedData = []byte{
		123, 34, 116, 101, 115, 116, 78, 97, 109, 101, 34, 58, 34, 77, 97, 116, 116, 104, 101, 119, 32, 77, 117, 114,
		100, 111, 99, 107, 34, 44, 34, 116, 101, 115, 116, 65, 103, 101, 34, 58, 50, 56, 44, 34, 84, 101, 115, 116,
		79, 116, 104, 101, 114, 83, 116, 114, 105, 110, 103, 34, 58, 34, 115, 111, 109, 101, 32, 79, 84, 72, 69, 82,
		32, 115, 116, 114, 105, 110, 103, 34, 125,
	}
	test.dataForTest.testStruct = testStructForMarshalerService{
		TestName:        "Matthew Murdock",
		TestAge:         28,
		TestOtherString: "some OTHER string",
	}
}

type loggerServiceTest struct {
	logger      loggerServiceInterface
	dataForTest struct {
		successfulLoggingDataTable []struct {
			message               string
			level                 int
			expectedLoggedMessage string
		}
	}
}

func TestLoggerService(t *testing.T) {
	test := loggerServiceTest{}
	test.prepareDataForTest()

	t.Run("testSuccessfulLoggingInDebugMode", test.testSuccessfulLoggingInDebugMode)
	t.Run("testNothingIsLoggedWithDebugModeOff", test.testNothingIsLoggedWithDebugModeOff)
}

func (test *loggerServiceTest) testSuccessfulLoggingInDebugMode(t *testing.T) {
	var testOutputBuffer bytes.Buffer
	log.SetOutput(&testOutputBuffer)
	test.logger = newLoggerService(true)
	for _, tableRow := range test.dataForTest.successfulLoggingDataTable {
		test.logger.log(tableRow.message, tableRow.level)
		assert.Regexp(t, tableRow.expectedLoggedMessage, testOutputBuffer.String())
	}
	log.SetOutput(os.Stdout)
}

func (test *loggerServiceTest) testNothingIsLoggedWithDebugModeOff(t *testing.T) {
	var testOutputBuffer bytes.Buffer
	log.SetOutput(&testOutputBuffer)
	test.logger = newLoggerService(false)
	for _, tableRow := range test.dataForTest.successfulLoggingDataTable {
		test.logger.log(tableRow.message, tableRow.level)
		assert.Nil(t, testOutputBuffer.Bytes())
	}
	log.SetOutput(os.Stdout)
}

func (test *loggerServiceTest) prepareDataForTest() {
	timeNowStringWithoutSeconds := time.Now().Format("2006/01/02 15:04")
	test.dataForTest.successfulLoggingDataTable = []struct {
		message               string
		level                 int
		expectedLoggedMessage string
	}{
		{
			message: "first Test message for debug log",
			level:   debugLevel,
			expectedLoggedMessage: fmt.Sprintf(
				"%s:[0-9]{2} %s",
				timeNowStringWithoutSeconds,
				`\(tmpstorage\)\[DEBUG\]: first Test message for debug log`,
			),
		},
		{
			message: "",
			level:   debugLevel,
			expectedLoggedMessage: fmt.Sprintf(
				"%s:[0-9]{2} %s",
				timeNowStringWithoutSeconds,
				`\(tmpstorage\)\[DEBUG\]: `,
			),
		},
		{
			message: "third error message",
			level:   errorLevel,
			expectedLoggedMessage: fmt.Sprintf(
				"%s:[0-9]{2} %s",
				timeNowStringWithoutSeconds,
				`\(tmpstorage\)\[ERROR\]: third error message`,
			),
		},
		{
			message: "",
			level:   errorLevel,
			expectedLoggedMessage: fmt.Sprintf(
				"%s:[0-9]{2} %s",
				timeNowStringWithoutSeconds,
				`\(tmpstorage\)\[ERROR\]: `,
			),
		},
		{
			message: "",
			level:   123, //invalid log level
			expectedLoggedMessage: fmt.Sprintf(
				"%s:[0-9]{2} %s",
				timeNowStringWithoutSeconds,
				`\(tmpstorage\): `,
			),
		},
	}
}

type createNowTimeServiceTest struct {
	createNowTimeService createNowTimeServiceInterface
	dataForTest          struct {
		expectedType                   time.Time
		expectedTruncatedToSecondsTime time.Time
	}
}

func TestCreateNowTimeServiceTest(t *testing.T) {
	serviceTest := new(createNowTimeServiceTest)
	serviceTest.createNowTimeService = newCreateNowTimeService()
	serviceTest.prepareDataForTest()

	t.Parallel()
	t.Run("testSuccessfulExecution", serviceTest.testSuccessfulExecution)
}

func (test *createNowTimeServiceTest) testSuccessfulExecution(t *testing.T) {
	nowTime := test.createNowTimeService.execute()

	assert.IsType(t, test.dataForTest.expectedType, nowTime)
	assert.Equal(t, test.dataForTest.expectedTruncatedToSecondsTime, nowTime.Truncate(time.Second))
}

func (test *createNowTimeServiceTest) prepareDataForTest() {
	test.dataForTest.expectedTruncatedToSecondsTime = time.Now().Truncate(time.Second)
}

type getBsonParameterNameForDataToStoreDtoServiceTest struct {
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface
	dataForTest                                  struct {
		successfulExecutionTable []struct {
			parameterID           *dataToStoreParameterID
			expectedParameterName string
		}
		expectedDataToStoreBsonD bson.D
		dataToStore              DataToStoreDto
		errorFromMapper          TraceableErrorInterface
		expectedPanicParameter   string
	}
	mocks struct {
		dataToStoreDtoToBsonDMapper *mockDataToStoreDtoToBsonDMapperInterface
	}
}

func TestGetBsonParameterNameForDataToStoreDtoService(t *testing.T) {
	test := getBsonParameterNameForDataToStoreDtoServiceTest{}
	test.prepareMocks()
	test.prepareDataForTest()
	test.getBsonParameterNameForDataToStoreDtoService = newGetBsonParameterNameForDataToStoreDtoService(
		test.mocks.dataToStoreDtoToBsonDMapper,
	)

	t.Parallel()
	t.Run("testSuccessfulExecution", test.testSuccessfulExecution)
	t.Run(
		"testPanicsOnErrorFromDataToStoreDtoToBsonDMapper",
		test.testPanicsOnErrorFromDataToStoreDtoToBsonDMapper,
	)
}

func (test *getBsonParameterNameForDataToStoreDtoServiceTest) testSuccessfulExecution(t *testing.T) {
	for _, tableRow := range test.dataForTest.successfulExecutionTable {
		dataToStoreBsonD := new(bson.D)
		test.mocks.dataToStoreDtoToBsonDMapper.
			On("executeMapping", test.dataForTest.dataToStore, dataToStoreBsonD).
			Return(nil).
			Once().
			Run(func(args mock.Arguments) {
				dataToStoreBsonDEmpty := args.Get(1).(*bson.D)
				*dataToStoreBsonDEmpty = test.dataForTest.expectedDataToStoreBsonD
			})

		actualParameterName := test.getBsonParameterNameForDataToStoreDtoService.execute(tableRow.parameterID)
		assert.Equal(t, tableRow.expectedParameterName, actualParameterName)
	}
}
func (test *getBsonParameterNameForDataToStoreDtoServiceTest) testPanicsOnErrorFromDataToStoreDtoToBsonDMapper(
	t *testing.T,
) {
	dataToStoreBsonD := new(bson.D)
	test.mocks.dataToStoreDtoToBsonDMapper.
		On("executeMapping", test.dataForTest.dataToStore, dataToStoreBsonD).
		Return(test.dataForTest.errorFromMapper).
		Once()

	assert.PanicsWithValue(t, test.dataForTest.expectedPanicParameter, func() {
		test.getBsonParameterNameForDataToStoreDtoService.execute(
			test.dataForTest.successfulExecutionTable[0].parameterID,
		)
	})
}
func (test *getBsonParameterNameForDataToStoreDtoServiceTest) prepareDataForTest() {
	test.dataForTest.successfulExecutionTable = []struct {
		parameterID           *dataToStoreParameterID
		expectedParameterName string
	}{
		{parameterID: newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID), expectedParameterName: "dataKey"},
		{parameterID: newDataToStoreParameterIDWithPanicOnError(dataParameterID), expectedParameterName: "data"},
		{parameterID: newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID), expectedParameterName: "expiresAt"},
	}
	test.dataForTest.expectedDataToStoreBsonD = bson.D{
		{Key: "dataKey", Value: ""},
		{Key: "data", Value: ""},
		{Key: "expiresAt", Value: ""},
	}
	test.dataForTest.dataToStore = DataToStoreDto{}
	test.dataForTest.errorFromMapper = newFailedToMapDataToStoreDtoToBson(errors.New(""))
	test.dataForTest.expectedPanicParameter = test.dataForTest.errorFromMapper.Error()
}

func (test *getBsonParameterNameForDataToStoreDtoServiceTest) prepareMocks() {
	test.mocks.dataToStoreDtoToBsonDMapper = new(mockDataToStoreDtoToBsonDMapperInterface)
}
