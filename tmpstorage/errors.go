package tmpstorage

import (
	"fmt"
	"reflect"
)

//TraceableErrorInterface is the error, which can contain information about previously
//occurred errors
type TraceableErrorInterface interface {
	Error() string

	//Previous makes it possible to retrieve previously occurred error
	Previous() error
}

type failedToDeleteDataFromStorageError struct {
	message  string
	previous error
}

func newFailedToDeleteDataFromStorageError(previousError error) *failedToDeleteDataFromStorageError {
	return &failedToDeleteDataFromStorageError{
		message:  "failed to delete data from storage due to an error. Check previous error from more info",
		previous: previousError,
	}
}

func (err *failedToDeleteDataFromStorageError) Error() string {
	return err.message
}

func (err *failedToDeleteDataFromStorageError) Previous() error {
	return err.previous
}

type failedToAddDataToStorageError struct {
	message  string
	previous error
}

func newFailedToAddDataToStorageError(previousError error) *failedToAddDataToStorageError {
	return &failedToAddDataToStorageError{
		message:  "failed to add data to storage due to an error. Check previous error from more info",
		previous: previousError,
	}
}

func (err *failedToAddDataToStorageError) Error() string {
	return err.message
}

func (err *failedToAddDataToStorageError) Previous() error {
	return err.previous
}

type failedToLoadDataFromStorageError struct {
	message       string
	previousError error
}

func newFailedToLoadDataFromStorageError(previousError error) *failedToLoadDataFromStorageError {
	return &failedToLoadDataFromStorageError{
		message:       "failed to load data from storage due to an error. Check previous error from more info",
		previousError: previousError,
	}
}

func (err *failedToLoadDataFromStorageError) Error() string {
	return err.message
}

func (err *failedToLoadDataFromStorageError) Previous() error {
	return err.previousError
}

type failedToMapDataToStoreDtoToBson struct {
	message  string
	previous error
}

func newFailedToMapDataToStoreDtoToBson(previousError error) *failedToMapDataToStoreDtoToBson {
	return &failedToMapDataToStoreDtoToBson{
		message: fmt.Sprintf(
			"failed to map %s to Bson due to an MarshalBSON error",
			reflect.TypeOf(DataToStoreDto{}).Name(),
		),
		previous: previousError,
	}
}

func (err *failedToMapDataToStoreDtoToBson) Error() string {
	return err.message
}

func (err *failedToMapDataToStoreDtoToBson) Previous() error {
	return err.previous
}

type failedToUpdateDataInStorageError struct {
	message  string
	previous error
}

func newFailedToUpdateDataInStorageError(previousError error) *failedToUpdateDataInStorageError {
	return &failedToUpdateDataInStorageError{
		message:  "failed to update data in storage due to an error. Check previous error from more info",
		previous: previousError,
	}
}

func (err *failedToUpdateDataInStorageError) Error() string {
	return err.message
}

func (err *failedToUpdateDataInStorageError) Previous() error {
	return err.previous
}

type invalidDataToStoreParameterIDError struct {
	message string
}

func newInvalidDataToStoreParameterID(parameterID int) *invalidDataToStoreParameterIDError {
	return &invalidDataToStoreParameterIDError{
		message: fmt.Sprintf("parameter ID '%d' is invalid", parameterID),
	}
}

func (err *invalidDataToStoreParameterIDError) Error() string {
	return err.message
}

type parameterIDIsNotSupportedError struct {
	message string
}

func newParameterIDIsNotSupportedError(
	parameterID int,
) *parameterIDIsNotSupportedError {
	return &parameterIDIsNotSupportedError{
		message: fmt.Sprintf("parameter ID '%d' is not supported", parameterID),
	}
}

func (err *parameterIDIsNotSupportedError) Error() string {
	return err.message
}
