package tmpstorage


type dataKeyDbIndexIsDtoIsValidSpecificationInterface interface {
	isSatisfiedBy(dto dataKeyDbIndexDto) bool
}

type dataKeyDbIndexIsDtoIsValidSpecification struct{}

const validDataKeyValue int = 1

func newDataKeyDbIndexIsDtoIsValidSpecification() *dataKeyDbIndexIsDtoIsValidSpecification {
	return &dataKeyDbIndexIsDtoIsValidSpecification{}
}

func (spec *dataKeyDbIndexIsDtoIsValidSpecification) isSatisfiedBy(dto dataKeyDbIndexDto) bool {
	return true == dto.Unique && validDataKeyValue == dto.Key.DataKey
}

type expiresAtDbIndexDtoIsValidSpecificationInterface interface {
	isSatisfiedBy(dto expiresAtDbIndexDto) bool
}

type expiresAtDbIndexDtoIsValidSpecification struct{}

const validExpiresAtValue int = 1

func newExpiresAtDbIndexDtoIsValidSpecification() *expiresAtDbIndexDtoIsValidSpecification {
	return &expiresAtDbIndexDtoIsValidSpecification{}
}

func (spec *expiresAtDbIndexDtoIsValidSpecification) isSatisfiedBy(dto expiresAtDbIndexDto) bool {
	return validExpiresAtValue == dto.Key.ExpiresAt
}
