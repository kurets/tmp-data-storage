package tmpstorage

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"os"
	"testing"
	"time"
)

type testDataType struct {
	name          string
	testSubStruct struct {
		subName string
		age     int
	}
}
type dataStorageTest struct {
	dataStorage *DataStorage
	dataForTest struct {
		databaseURL                      string
		databaseName                     string
		connector                        dbConnector
		expectedInitializationLogMessage string
		dataKey                          string
		filledDataPointer                testDataType
		expectedTryingToAddLogMessage    string
		expectedTryingToUpdateLogMessage string
		errorOnUpdating                  TraceableErrorInterface
		errorOnAdding                    TraceableErrorInterface
	}
	mocks struct {
		dataToStoreDtoFactory        *MockDataToStoreDtoFactoryInterface
		addDataToStorageService      *mockAddDataToStorageServiceInterface
		loadDataFromStorageService   *mockLoadDataFromStorageServiceInterface
		deleteDataFromStorageService *mockDeleteDataFromStorageServiceInterface
		updateDataInStorageService   *mockUpdateDataInStorageServiceInterface
		logger                       *mockLoggerServiceInterface
		expectedTraceableError       *MockTraceableErrorInterface
	}
}

func TestDataStorageService(t *testing.T) {
	test := dataStorageTest{}
	test.prepareDataForTest()
	test.prepareMocks()
	test.dataStorage = newDataStorage(
		test.mocks.dataToStoreDtoFactory,
		test.mocks.addDataToStorageService,
		test.mocks.loadDataFromStorageService,
		test.mocks.deleteDataFromStorageService,
		test.mocks.updateDataInStorageService,
		test.mocks.logger,
	)

	t.Parallel()
	t.Run("testConstructionOfDataStorage", test.testConstructionOfDataStorage)
	t.Run("testSuccessfulInitializationOfDataStorage", test.testSuccessfulInitializationOfDataStorage)
	t.Run("testErrorOnInitializeDataStorage", test.testErrorOnInitializeDataStorage)
	t.Run("testSuccessfulAdding", test.testSuccessfulAdding)
	t.Run("testErrorOnAdding", test.testErrorOnAdding)
	t.Run("testSuccessfulLoading", test.testSuccessfulLoading)
	t.Run("testErrorOnLoading", test.testErrorOnLoading)
	t.Run("testSuccessfulDeletion", test.testSuccessfulDeletion)
	t.Run("testErrorOnDeletion", test.testErrorOnDeletion)
	t.Run("testSuccessfulUpdating", test.testSuccessfulUpdating)
	t.Run("testErrorOnUpdating", test.testErrorOnUpdating)
	t.Run("testSuccessfulAddingOnAddOrUpdate", test.testSuccessfulAddingOnAddOrUpdate)
	t.Run("testSuccessfulUpdatingOnAddOrUpdate", test.testSuccessfulUpdatingOnAddOrUpdate)
	t.Run("testErrorOnAddOrUpdate", test.testErrorOnAddOrUpdate)
}

func (test *dataStorageTest) testConstructionOfDataStorage(t *testing.T) {
	service := newDataStorage(
		test.mocks.dataToStoreDtoFactory,
		test.mocks.addDataToStorageService,
		test.mocks.loadDataFromStorageService,
		test.mocks.deleteDataFromStorageService,
		test.mocks.updateDataInStorageService,
		test.mocks.logger,
	)
	assert.NotNil(t, service)
	assert.NotNil(t, service.DataToStoreDtoFactory)
	assert.NotNil(t, service.addDataToStorageService)
	assert.NotNil(t, service.loadDataFromStorageService)
	assert.NotNil(t, service.deleteDataFromStorageService)
	assert.NotNil(t, service.updateDataInStorageService)
	assert.NotNil(t, service.logger)
}

func (test *dataStorageTest) testSuccessfulInitializationOfDataStorage(t *testing.T) {
	var testOutputBuffer bytes.Buffer
	log.SetOutput(&testOutputBuffer)
	storage, err := InitializeDataStorage(
		test.dataForTest.databaseURL,
		test.dataForTest.databaseName,
		true,
	)
	log.SetOutput(os.Stdout)

	assert.Regexp(t, test.dataForTest.expectedInitializationLogMessage, testOutputBuffer.String())
	assert.NotNil(t, storage)
	assert.Nil(t, err)
	assert.NotNil(t, storage.DataToStoreDtoFactory)
	assert.NotNil(t, storage.addDataToStorageService)
	assert.NotNil(t, storage.loadDataFromStorageService)
	assert.NotNil(t, storage.deleteDataFromStorageService)
	assert.NotNil(t, storage.updateDataInStorageService)
	assert.NotNil(t, storage.logger)
}

func (test *dataStorageTest) testErrorOnInitializeDataStorage(t *testing.T) {
	setDbConnectorToMockWithErrorMode()
	defer setDbConnectorToMockMode()
	var testOutputBuffer bytes.Buffer
	log.SetOutput(&testOutputBuffer)
	storage, err := InitializeDataStorage(
		test.dataForTest.databaseURL,
		test.dataForTest.databaseName,
		true,
	)
	log.SetOutput(os.Stdout)

	assert.Empty(t, testOutputBuffer.String())
	assert.Nil(t, storage)
	assert.Error(t, err)
	assert.EqualError(t, err, "database connector error")
}

func (test *dataStorageTest) testSuccessfulAdding(t *testing.T) {
	dataToStoreDto := DataToStoreDto{}
	test.mocks.addDataToStorageService.
		On("executeAdding", dataToStoreDto).
		Return(nil).
		Once()

	actualError := test.dataStorage.Add(dataToStoreDto)
	assert.Nil(t, actualError)
}

func (test *dataStorageTest) testErrorOnAdding(t *testing.T) {
	dataToStoreDto := DataToStoreDto{}
	test.mocks.addDataToStorageService.
		On("executeAdding", dataToStoreDto).
		Return(test.mocks.expectedTraceableError).
		Once()

	actualError := test.dataStorage.Add(dataToStoreDto)

	assert.Error(t, actualError)
	assert.Equal(t, test.mocks.expectedTraceableError, actualError)
}

func (test *dataStorageTest) testSuccessfulLoading(t *testing.T) {
	testDataTypePointer := new(testDataType)
	test.mocks.loadDataFromStorageService.
		On("executeLoading", test.dataForTest.dataKey, testDataTypePointer).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			testDataTypePointer := args.Get(1).(*testDataType)
			*testDataTypePointer = test.dataForTest.filledDataPointer
		})

	actualError := test.dataStorage.Load(test.dataForTest.dataKey, testDataTypePointer)

	assert.Nil(t, actualError)
	assert.Equal(t, &test.dataForTest.filledDataPointer, testDataTypePointer)
}

func (test *dataStorageTest) testErrorOnLoading(t *testing.T) {
	testDataTypePointer := new(testDataType)
	test.mocks.loadDataFromStorageService.
		On("executeLoading", test.dataForTest.dataKey, testDataTypePointer).
		Return(test.mocks.expectedTraceableError).
		Once()

	actualError := test.dataStorage.Load(test.dataForTest.dataKey, testDataTypePointer)
	assert.Error(t, actualError)
	assert.Equal(t, test.mocks.expectedTraceableError, actualError)
}

func (test *dataStorageTest) testSuccessfulDeletion(t *testing.T) {
	testDataTypePointer := testDataType{}
	test.mocks.deleteDataFromStorageService.
		On("executeDeletion", test.dataForTest.dataKey, testDataTypePointer).
		Return(nil).
		Once()

	actualError := test.dataStorage.Delete(test.dataForTest.dataKey, testDataTypePointer)

	assert.Nil(t, actualError)
}

func (test *dataStorageTest) testErrorOnDeletion(t *testing.T) {
	testDataTypePointer := testDataType{}
	test.mocks.deleteDataFromStorageService.
		On("executeDeletion", test.dataForTest.dataKey, testDataTypePointer).
		Return(test.mocks.expectedTraceableError).
		Once()

	actualError := test.dataStorage.Delete(test.dataForTest.dataKey, testDataTypePointer)

	assert.Error(t, actualError)
	assert.Equal(t, test.mocks.expectedTraceableError, actualError)
}

func (test *dataStorageTest) testSuccessfulUpdating(t *testing.T) {
	dataToStoreDto := DataToStoreDto{}
	test.mocks.updateDataInStorageService.
		On("executeUpdating", dataToStoreDto).
		Return(nil).
		Once()

	actualError := test.dataStorage.Update(dataToStoreDto)
	assert.Nil(t, actualError)
}

func (test *dataStorageTest) testErrorOnUpdating(t *testing.T) {
	dataToStoreDto := DataToStoreDto{}
	test.mocks.updateDataInStorageService.
		On("executeUpdating", dataToStoreDto).
		Return(test.mocks.expectedTraceableError).
		Once()

	actualError := test.dataStorage.Update(dataToStoreDto)

	assert.Error(t, actualError)
	assert.Equal(t, test.mocks.expectedTraceableError, actualError)
}

func (test *dataStorageTest) testSuccessfulAddingOnAddOrUpdate(t *testing.T) {
	dataToStoreDto := DataToStoreDto{}
	test.mocks.logger.
		On("log", test.dataForTest.expectedTryingToAddLogMessage, debugLevel).
		Once()

	test.mocks.addDataToStorageService.
		On("executeAdding", dataToStoreDto).
		Return(nil).
		Once()

	test.mocks.updateDataInStorageService.AssertNotCalled(t, "executeUpdating")

	actualError := test.dataStorage.AddOrUpdate(dataToStoreDto)
	assert.Nil(t, actualError)
}

func (test *dataStorageTest) testSuccessfulUpdatingOnAddOrUpdate(t *testing.T) {
	dataToStoreDto := DataToStoreDto{}
	test.mocks.logger.ExpectedCalls = []*mock.Call{
		{
			Method:          "log",
			Arguments:       mock.Arguments{test.dataForTest.expectedTryingToAddLogMessage, debugLevel},
			ReturnArguments: nil,
		}, {
			Method:          "log",
			Arguments:       mock.Arguments{test.dataForTest.expectedTryingToUpdateLogMessage, debugLevel},
			ReturnArguments: nil,
		},
	}

	test.mocks.addDataToStorageService.
		On("executeAdding", dataToStoreDto).
		Return(test.mocks.expectedTraceableError).
		Once()

	test.mocks.updateDataInStorageService.
		On("executeUpdating", dataToStoreDto).
		Return(nil).
		Once()

	actualError := test.dataStorage.AddOrUpdate(dataToStoreDto)
	assert.Nil(t, actualError)
}

func (test *dataStorageTest) testErrorOnAddOrUpdate(t *testing.T) {
	dataToStoreDto := DataToStoreDto{}
	test.mocks.logger.ExpectedCalls = []*mock.Call{
		{
			Method:          "log",
			Arguments:       mock.Arguments{test.dataForTest.expectedTryingToAddLogMessage, debugLevel},
			ReturnArguments: nil,
		}, {
			Method:          "log",
			Arguments:       mock.Arguments{test.dataForTest.expectedTryingToUpdateLogMessage, debugLevel},
			ReturnArguments: nil,
		},
	}

	test.mocks.addDataToStorageService.
		On("executeAdding", dataToStoreDto).
		Return(test.dataForTest.errorOnAdding).
		Once()

	test.mocks.updateDataInStorageService.
		On("executeUpdating", dataToStoreDto).
		Return(test.dataForTest.errorOnUpdating).
		Once()

	actualError := test.dataStorage.AddOrUpdate(dataToStoreDto)
	assert.Error(t, actualError)
	assert.NotEqual(t, test.dataForTest.errorOnAdding, actualError)
	assert.Equal(t, test.dataForTest.errorOnUpdating, actualError)
}

func (test *dataStorageTest) prepareDataForTest() {
	test.dataForTest.databaseURL = "this/is/db/url"
	test.dataForTest.databaseName = "databaseName"

	test.dataForTest.connector = func(
		ctx context.Context,
		opts ...*options.ClientOptions,
	) (client *mongo.Client, err error) {
		return new(mongo.Client), nil
	}

	timeNowStringWithoutSeconds := time.Now().Format("2006/01/02 15:04")
	test.dataForTest.expectedInitializationLogMessage = fmt.Sprintf(
		"%s:[0-9]{2} %s",
		timeNowStringWithoutSeconds,
		`Connected to storage database`,
	)
	test.dataForTest.dataKey = "dataKEy123456"
	test.dataForTest.filledDataPointer = testDataType{
		name: "Obi Wan",
		testSubStruct: struct {
			subName string
			age     int
		}{
			subName: "Anakin Skywalker",
			age:     6,
		},
	}
	test.dataForTest.expectedTryingToAddLogMessage = "Trying to use 'Add' method on dataToStoreDto"
	test.dataForTest.expectedTryingToUpdateLogMessage = "Trying to update dataToStoreDto after 'Add' method failure"
	test.dataForTest.errorOnAdding = newFailedToAddDataToStorageError(errors.New("some adding error"))
	test.dataForTest.errorOnUpdating = newFailedToUpdateDataInStorageError(errors.New("some updating error"))
}

func (test *dataStorageTest) prepareMocks() {
	test.mocks.dataToStoreDtoFactory = new(MockDataToStoreDtoFactoryInterface)
	test.mocks.addDataToStorageService = new(mockAddDataToStorageServiceInterface)
	test.mocks.loadDataFromStorageService = new(mockLoadDataFromStorageServiceInterface)
	test.mocks.deleteDataFromStorageService = new(mockDeleteDataFromStorageServiceInterface)
	test.mocks.updateDataInStorageService = new(mockUpdateDataInStorageServiceInterface)
	test.mocks.logger = new(mockLoggerServiceInterface)
	test.mocks.expectedTraceableError = new(MockTraceableErrorInterface)
}

type addDataToStorageServiceTest struct {
	addDataToStorageService addDataToStorageServiceInterface
	mocks                   struct {
		getCollectionForDataService          *mockGetCollectionForDataServiceInterface
		createIndexIfNotExistsServiceFactory *mockCreateIndexIfNotExistsServiceFactoryInterface
		logger                               *mockLoggerServiceInterface
		createTTLIndexService                *mockCreateIndexIfNotExistsServiceInterface
		createUniqueIndexService             *mockCreateIndexIfNotExistsServiceInterface
		dbCollection                         *mockDbCollectionInterface
	}
	dataForTest struct {
		dataToStoreDto       DataToStoreDto
		dataKeyParameterID   *dataToStoreParameterID
		expiresAtParameterID *dataToStoreParameterID
		successfulLogMessage string
		debugLevel           int
		errorLevel           int
		errorFromDb          error
		errorLogMessage      string
		expectedError        TraceableErrorInterface
	}
}

func TestAddDataToStorageService(t *testing.T) {
	serviceTest := new(addDataToStorageServiceTest)
	serviceTest.prepareMocks()
	serviceTest.prepareDataForTest()

	serviceTest.addDataToStorageService = newAddDataToStorageService(
		serviceTest.mocks.getCollectionForDataService,
		serviceTest.mocks.createIndexIfNotExistsServiceFactory,
		serviceTest.mocks.logger,
	)

	t.Parallel()
	t.Run("testSuccessfulAdding", serviceTest.testSuccessfulAdding)
	t.Run("testFailureOnInsertOne", serviceTest.testFailureOnInsertOne)
}

func (test *addDataToStorageServiceTest) testSuccessfulAdding(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToStoreDto.Data).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.createIndexIfNotExistsServiceFactory.
		On("create", test.dataForTest.expiresAtParameterID).
		Return(test.mocks.createTTLIndexService).
		Once()

	test.mocks.createTTLIndexService.
		On("execute", test.mocks.dbCollection).
		Once()

	test.mocks.createIndexIfNotExistsServiceFactory.
		On("create", test.dataForTest.dataKeyParameterID).
		Return(test.mocks.createUniqueIndexService).
		Once()

	test.mocks.createUniqueIndexService.
		On("execute", test.mocks.dbCollection).
		Once()

	test.mocks.dbCollection.
		On("InsertOne", context.TODO(), test.dataForTest.dataToStoreDto).
		Return(new(mongo.InsertOneResult), nil).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.successfulLogMessage, test.dataForTest.debugLevel).
		Once()

	err := test.addDataToStorageService.executeAdding(test.dataForTest.dataToStoreDto)

	assert.Nil(t, err)

}

func (test *addDataToStorageServiceTest) testFailureOnInsertOne(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToStoreDto.Data).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.createIndexIfNotExistsServiceFactory.
		On("create", test.dataForTest.expiresAtParameterID).
		Return(test.mocks.createTTLIndexService).
		Once()

	test.mocks.createTTLIndexService.
		On("execute", test.mocks.dbCollection).
		Once()

	test.mocks.createIndexIfNotExistsServiceFactory.
		On("create", test.dataForTest.dataKeyParameterID).
		Return(test.mocks.createUniqueIndexService).
		Once()

	test.mocks.createUniqueIndexService.
		On("execute", test.mocks.dbCollection).
		Once()

	test.mocks.dbCollection.
		On("InsertOne", context.TODO(), test.dataForTest.dataToStoreDto).
		Return(new(mongo.InsertOneResult), test.dataForTest.errorFromDb).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.errorLogMessage, test.dataForTest.errorLevel).
		Once()

	err := test.addDataToStorageService.executeAdding(test.dataForTest.dataToStoreDto)

	assert.Error(t, err)
	assert.Equal(t, test.dataForTest.expectedError, err)
	assert.EqualError(t, err, test.dataForTest.expectedError.Error())
	assert.Error(t, err.Previous())
	assert.EqualError(t, err.Previous(), test.dataForTest.errorFromDb.Error())
}

func (test *addDataToStorageServiceTest) prepareMocks() {
	test.mocks.getCollectionForDataService = new(mockGetCollectionForDataServiceInterface)
	test.mocks.createIndexIfNotExistsServiceFactory = new(mockCreateIndexIfNotExistsServiceFactoryInterface)
	test.mocks.logger = new(mockLoggerServiceInterface)
	test.mocks.createTTLIndexService = new(mockCreateIndexIfNotExistsServiceInterface)
	test.mocks.createUniqueIndexService = new(mockCreateIndexIfNotExistsServiceInterface)
	test.mocks.dbCollection = new(mockDbCollectionInterface)
}

func (test *addDataToStorageServiceTest) prepareDataForTest() {
	test.dataForTest.dataToStoreDto.DataKey = "testDataKey"
	test.dataForTest.dataKeyParameterID = newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	test.dataForTest.expiresAtParameterID = newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID)
	test.dataForTest.successfulLogMessage = fmt.Sprintf(
		"Data with key '%s' has been added to storage",
		test.dataForTest.dataToStoreDto.DataKey,
	)
	test.dataForTest.debugLevel = debugLevel
	test.dataForTest.errorLevel = errorLevel
	test.dataForTest.errorFromDb = errors.New("error when inserting")
	test.dataForTest.errorLogMessage = fmt.Sprintf(
		"Failed to add data with key '%s' to storage due to an error '%s'",
		test.dataForTest.dataToStoreDto.DataKey,
		test.dataForTest.errorFromDb,
	)
	test.dataForTest.expectedError = newFailedToAddDataToStorageError(test.dataForTest.errorFromDb)
}

type loadDataFromStorageServiceTest struct {
	loadDataFromStorageService loadDataFromStorageServiceInterface
	mocks                      struct {
		getCollectionForDataService                  *mockGetCollectionForDataServiceInterface
		getBsonParameterNameForDataToStoreDtoService *mockGetBsonParameterNameForDataToStoreDtoServiceInterface
		marshalerService                             *mockMarshalerServiceInterface
		logger                                       *mockLoggerServiceInterface
		dbCollection                                 *mockDbCollectionInterface
		dbFindOneResultToBsonDMapper                 *mockDbFindOneResultToBsonDMapperInterface
	}
	dataForTest struct {
		dataKey                   string
		dataPointerMapped         testDataPointerStruct
		dataKeyParameterIDInt     int
		dataKeyParameterID        *dataToStoreParameterID
		dataKeyDbFieldName        string
		findOneFilter             bson.M
		findOneSingleResult       *mongo.SingleResult
		resultBsonDEmpty          bson.D
		resultBsonDMapped         bson.D
		dataInByteSlice           []byte
		dataFromResultBsonMEmpty  bson.M
		dataFromResultBsonMMapped bson.M
		successfulLogMessage      string
		errorLogMessage           string
		logDebugLevel             int
		logErrorLevel             int
		errorOccurred             error
		expectedError             TraceableErrorInterface
	}
}

type testDataPointerStruct struct {
	name  string
	email string
	age   int
}

func TestLoadDataFromStorageService(t *testing.T) {
	serviceTest := new(loadDataFromStorageServiceTest)
	serviceTest.prepareDataForTest()
	serviceTest.prepareMocks()
	serviceTest.loadDataFromStorageService = newLoadDataFromStorageService(
		serviceTest.mocks.getCollectionForDataService,
		serviceTest.mocks.getBsonParameterNameForDataToStoreDtoService,
		serviceTest.mocks.marshalerService,
		serviceTest.mocks.dbFindOneResultToBsonDMapper,
		serviceTest.mocks.logger,
	)

	t.Parallel()
	t.Run("testSuccessfulDataLoadingFromCache", serviceTest.testSuccessfulDataLoadingFromCache)
	t.Run("testPanicOnGettingDataKeyFieldName", serviceTest.testPanicOnGettingDataKeyFieldName)
	t.Run(
		"testErrorIsReturnedOnResultFromDbMappingFailure",
		serviceTest.testErrorIsReturnedOnResultFromDbMappingFailure,
	)
	t.Run("testPanicOnMarshalBSONData", serviceTest.testPanicOnMarshalBSONData)
	t.Run("testPanicOnUnmarshalRawBsonDataToBsonM", serviceTest.testPanicOnUnmarshalRawBsonDataToBsonM)
	t.Run("testPanicOnMarshalJSONData", serviceTest.testPanicOnMarshalJSONData)
	t.Run(
		"testPanicOnUnmarshalRawJSONDataToDataPointer",
		serviceTest.testPanicOnUnmarshalRawJSONDataToDataPointer,
	)
}

func (test *loadDataFromStorageServiceTest) testSuccessfulDataLoadingFromCache(t *testing.T) {
	dataPointer := new(testDataPointerStruct)
	test.mocks.getCollectionForDataService.
		On("execute", dataPointer).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbFieldName, nil).
		Once()

	test.mocks.dbCollection.
		On("FindOne", context.TODO(), test.dataForTest.findOneFilter).
		Return(test.dataForTest.findOneSingleResult).
		Once()

	test.mocks.dbFindOneResultToBsonDMapper.
		On("executeMapping", test.dataForTest.findOneSingleResult, &test.dataForTest.resultBsonDEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.D)
			*arg = test.dataForTest.resultBsonDMapped
		})

	test.mocks.marshalerService.
		On("marshalBSON", test.dataForTest.resultBsonDMapped[2].Value).
		Return(test.dataForTest.dataInByteSlice, nil).
		Once()

	test.mocks.marshalerService.
		On("unmarshalBSON", test.dataForTest.dataInByteSlice, &test.dataForTest.dataFromResultBsonMEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.M)
			*arg = test.dataForTest.dataFromResultBsonMMapped
		})

	test.mocks.marshalerService.
		On("marshalJSON", &test.dataForTest.dataFromResultBsonMMapped).
		Return(test.dataForTest.dataInByteSlice, nil).
		Once()

	test.mocks.marshalerService.
		On("unmarshalJSON", test.dataForTest.dataInByteSlice, dataPointer).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			dataPointer := args.Get(1).(*testDataPointerStruct)
			dataPointer.age = test.dataForTest.dataPointerMapped.age
			dataPointer.email = test.dataForTest.dataPointerMapped.email
			dataPointer.name = test.dataForTest.dataPointerMapped.name
		})

	test.mocks.logger.
		On("log", test.dataForTest.successfulLogMessage, test.dataForTest.logDebugLevel).
		Once()

	errorActual := test.loadDataFromStorageService.executeLoading(
		test.dataForTest.dataKey,
		dataPointer,
	)

	assert.Equal(t, test.dataForTest.dataPointerMapped, *dataPointer)
	assert.Nil(t, errorActual)
}

func (test *loadDataFromStorageServiceTest) testPanicOnGettingDataKeyFieldName(t *testing.T) {
	dataPointer := new(testDataPointerStruct)
	test.mocks.getCollectionForDataService.
		On("execute", dataPointer).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return("", test.dataForTest.errorOccurred).
		Once()

	var actualError error
	assert.Panics(t, func() {
		actualError = test.loadDataFromStorageService.executeLoading(test.dataForTest.dataKey, dataPointer)
	},
		test.dataForTest.errorOccurred.Error(),
	)
	assert.Nil(t, actualError)
	assert.NotEqual(t, *dataPointer, test.dataForTest.dataPointerMapped)
}

func (test *loadDataFromStorageServiceTest) testErrorIsReturnedOnResultFromDbMappingFailure(t *testing.T) {
	dataPointer := new(testDataPointerStruct)
	test.mocks.getCollectionForDataService.
		On("execute", dataPointer).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbFieldName, nil).
		Once()

	test.mocks.dbCollection.
		On("FindOne", context.TODO(), test.dataForTest.findOneFilter).
		Return(test.dataForTest.findOneSingleResult).
		Once()

	test.mocks.dbFindOneResultToBsonDMapper.
		On("executeMapping", test.dataForTest.findOneSingleResult, &test.dataForTest.resultBsonDEmpty).
		Return(test.dataForTest.errorOccurred).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.errorLogMessage, test.dataForTest.logErrorLevel).
		Once()

	errorActual := test.loadDataFromStorageService.executeLoading(
		test.dataForTest.dataKey,
		dataPointer,
	)

	assert.Error(t, errorActual)
	assert.Equal(t, test.dataForTest.expectedError, errorActual)
	assert.EqualError(t, errorActual, test.dataForTest.expectedError.Error())
	assert.Error(t, errorActual.Previous())
	assert.EqualError(t, errorActual.Previous(), test.dataForTest.errorOccurred.Error())
	assert.NotEqual(t, *dataPointer, test.dataForTest.dataPointerMapped)
}

func (test *loadDataFromStorageServiceTest) testPanicOnMarshalBSONData(t *testing.T) {
	dataPointer := new(testDataPointerStruct)
	test.mocks.getCollectionForDataService.
		On("execute", dataPointer).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbFieldName, nil).
		Once()

	test.mocks.dbCollection.
		On("FindOne", context.TODO(), test.dataForTest.findOneFilter).
		Return(test.dataForTest.findOneSingleResult).
		Once()

	test.mocks.dbFindOneResultToBsonDMapper.
		On("executeMapping", test.dataForTest.findOneSingleResult, &test.dataForTest.resultBsonDEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.D)
			*arg = test.dataForTest.resultBsonDMapped
		})

	test.mocks.marshalerService.
		On("marshalBSON", test.dataForTest.resultBsonDMapped[2].Value).
		Return([]byte{}, test.dataForTest.errorOccurred).
		Once()

	var actualError error
	assert.Panics(t, func() {
		actualError = test.loadDataFromStorageService.executeLoading(test.dataForTest.dataKey, dataPointer)
	},
		test.dataForTest.errorOccurred.Error(),
	)
	assert.Nil(t, actualError)
	assert.NotEqual(t, *dataPointer, test.dataForTest.dataPointerMapped)
}

func (test *loadDataFromStorageServiceTest) testPanicOnUnmarshalRawBsonDataToBsonM(t *testing.T) {
	dataPointer := new(testDataPointerStruct)
	test.mocks.getCollectionForDataService.
		On("execute", dataPointer).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbFieldName, nil).
		Once()

	test.mocks.dbCollection.
		On("FindOne", context.TODO(), test.dataForTest.findOneFilter).
		Return(test.dataForTest.findOneSingleResult).
		Once()

	test.mocks.dbFindOneResultToBsonDMapper.
		On("executeMapping", test.dataForTest.findOneSingleResult, &test.dataForTest.resultBsonDEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.D)
			*arg = test.dataForTest.resultBsonDMapped
		})

	test.mocks.marshalerService.
		On("marshalBSON", test.dataForTest.resultBsonDMapped[2].Value).
		Return(test.dataForTest.dataInByteSlice, nil).
		Once()

	test.mocks.marshalerService.
		On("unmarshalBSON", test.dataForTest.dataInByteSlice, &test.dataForTest.dataFromResultBsonMEmpty).
		Return(test.dataForTest.errorOccurred).
		Once()

	var actualError error
	assert.Panics(t, func() {
		actualError = test.loadDataFromStorageService.executeLoading(test.dataForTest.dataKey, dataPointer)
	},
		test.dataForTest.errorOccurred.Error(),
	)
	assert.Nil(t, actualError)
	assert.NotEqual(t, *dataPointer, test.dataForTest.dataPointerMapped)
}

func (test *loadDataFromStorageServiceTest) testPanicOnMarshalJSONData(t *testing.T) {
	dataPointer := new(testDataPointerStruct)
	test.mocks.getCollectionForDataService.
		On("execute", dataPointer).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbFieldName, nil).
		Once()

	test.mocks.dbCollection.
		On("FindOne", context.TODO(), test.dataForTest.findOneFilter).
		Return(test.dataForTest.findOneSingleResult).
		Once()

	test.mocks.dbFindOneResultToBsonDMapper.
		On("executeMapping", test.dataForTest.findOneSingleResult, &test.dataForTest.resultBsonDEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.D)
			*arg = test.dataForTest.resultBsonDMapped
		})

	test.mocks.marshalerService.
		On("marshalBSON", test.dataForTest.resultBsonDMapped[2].Value).
		Return(test.dataForTest.dataInByteSlice, nil).
		Once()

	test.mocks.marshalerService.
		On("unmarshalBSON", test.dataForTest.dataInByteSlice, &test.dataForTest.dataFromResultBsonMEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.M)
			*arg = test.dataForTest.dataFromResultBsonMMapped
		})

	test.mocks.marshalerService.
		On("marshalJSON", &test.dataForTest.dataFromResultBsonMMapped).
		Return([]byte{}, test.dataForTest.errorOccurred).
		Once()

	var actualError error
	assert.Panics(t, func() {
		actualError = test.loadDataFromStorageService.executeLoading(test.dataForTest.dataKey, dataPointer)
	},
		test.dataForTest.errorOccurred.Error(),
	)
	assert.Nil(t, actualError)
	assert.NotEqual(t, *dataPointer, test.dataForTest.dataPointerMapped)
}

func (test *loadDataFromStorageServiceTest) testPanicOnUnmarshalRawJSONDataToDataPointer(t *testing.T) {
	dataPointer := new(testDataPointerStruct)
	test.mocks.getCollectionForDataService.
		On("execute", dataPointer).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbFieldName, nil).
		Once()

	test.mocks.dbCollection.
		On("FindOne", context.TODO(), test.dataForTest.findOneFilter).
		Return(test.dataForTest.findOneSingleResult).
		Once()

	test.mocks.dbFindOneResultToBsonDMapper.
		On("executeMapping", test.dataForTest.findOneSingleResult, &test.dataForTest.resultBsonDEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.D)
			*arg = test.dataForTest.resultBsonDMapped
		})

	test.mocks.marshalerService.
		On("marshalBSON", test.dataForTest.resultBsonDMapped[2].Value).
		Return(test.dataForTest.dataInByteSlice, nil).
		Once()

	test.mocks.marshalerService.
		On("unmarshalBSON", test.dataForTest.dataInByteSlice, &test.dataForTest.dataFromResultBsonMEmpty).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*bson.M)
			*arg = test.dataForTest.dataFromResultBsonMMapped
		})

	test.mocks.marshalerService.
		On("marshalJSON", &test.dataForTest.dataFromResultBsonMMapped).
		Return(test.dataForTest.dataInByteSlice, nil).
		Once()

	test.mocks.marshalerService.
		On("unmarshalJSON", test.dataForTest.dataInByteSlice, dataPointer).
		Return(test.dataForTest.errorOccurred).
		Once()

	var actualError error
	assert.Panics(t, func() {
		actualError = test.loadDataFromStorageService.executeLoading(test.dataForTest.dataKey, dataPointer)
	},
		test.dataForTest.errorOccurred.Error(),
	)
	assert.Nil(t, actualError)
	assert.NotEqual(t, *dataPointer, test.dataForTest.dataPointerMapped)
}

func (test *loadDataFromStorageServiceTest) prepareMocks() {
	test.mocks.getCollectionForDataService = new(mockGetCollectionForDataServiceInterface)
	test.mocks.getBsonParameterNameForDataToStoreDtoService = new(
		mockGetBsonParameterNameForDataToStoreDtoServiceInterface,
	)
	test.mocks.marshalerService = new(mockMarshalerServiceInterface)
	test.mocks.logger = new(mockLoggerServiceInterface)
	test.mocks.dbCollection = new(mockDbCollectionInterface)
	test.mocks.dbFindOneResultToBsonDMapper = new(mockDbFindOneResultToBsonDMapperInterface)
}

func (test *loadDataFromStorageServiceTest) prepareDataForTest() {
	test.dataForTest.dataPointerMapped = testDataPointerStruct{
		name:  "Matthew Murdock",
		email: "daredevil@marvel.com",
		age:   28,
	}
	test.dataForTest.dataKey = "testDataKey"
	test.dataForTest.dataKeyParameterID = newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	test.dataForTest.dataKeyDbFieldName = "dataKey"
	test.dataForTest.findOneFilter = bson.M{test.dataForTest.dataKeyDbFieldName: test.dataForTest.dataKey}
	test.dataForTest.findOneSingleResult = new(mongo.SingleResult)
	test.dataForTest.resultBsonDMapped = bson.D{
		{Key: "_id", Value: "1234567890"},
		{Key: "dataKey", Value: "QWERTY"},
		{Key: "data", Value: "some data"},
	}
	test.dataForTest.dataFromResultBsonMMapped = bson.M{
		"_id":     "1234567890",
		"dataKey": "QWERTY",
		"data":    "some data",
	}
	test.dataForTest.dataInByteSlice = []byte("some data")
	test.dataForTest.successfulLogMessage = "Data has been successfully loaded from storage"
	test.dataForTest.errorOccurred = errors.New("some error occurred")
	test.dataForTest.errorLogMessage = fmt.Sprintf(
		"Failed to load dataPointerMapped from storage due to an error: %s",
		test.dataForTest.errorOccurred,
	)
	test.dataForTest.expectedError = newFailedToLoadDataFromStorageError(test.dataForTest.errorOccurred)
	test.dataForTest.logDebugLevel = debugLevel
	test.dataForTest.logErrorLevel = errorLevel
}

type deleteDataFromStorageServiceTest struct {
	deleteDataFromStorageService deleteDataFromStorageServiceInterface
	dataForTest                  struct {
		dataToDelete struct {
			key   string
			value struct {
				id   int
				name string
			}
		}
		dataKey                          string
		dataKeyParameterID               *dataToStoreParameterID
		dataParameterID                  *dataToStoreParameterID
		dataKeyDbField                   string
		dataDbField                      string
		bsonMForDeletion                 bson.M
		mongoDeleteResultWithCountOne    *mongo.DeleteResult
		mongoDeleteResultWithCountZero   *mongo.DeleteResult
		successfulLogMessage             string
		logDebugLevel                    int
		dbError                          error
		nothingDeletedError              error
		expectedErrorAfterDbFailure      TraceableErrorInterface
		expectedErrorAfterNothingDeleted TraceableErrorInterface
		dbErrorLogMessage                string
		nothingDeletedErrorLogMessage    string
		logErrorLevel                    int
	}
	mocks struct {
		getCollectionForDataService                  *mockGetCollectionForDataServiceInterface
		getBsonParameterNameForDataToStoreDtoService *mockGetBsonParameterNameForDataToStoreDtoServiceInterface
		logger                                       *mockLoggerServiceInterface
		dbCollection                                 *mockDbCollectionInterface
	}
}

func TestDeleteDataFromStorageService(t *testing.T) {
	test := deleteDataFromStorageServiceTest{}
	test.prepareDataForTest()
	test.prepareMocks()
	test.deleteDataFromStorageService = newDeleteDataFromStorageService(
		test.mocks.getCollectionForDataService,
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.mocks.logger,
	)

	t.Parallel()
	t.Run("testSuccessfulExecution", test.testSuccessfulExecution)
	t.Run("testFailureOnDeletionFromDb", test.testFailureOnDeletionFromDb)
	t.Run("testFailureOnNothingHasBeenDeleted", test.testFailureOnNothingHasBeenDeleted)
}

func (test *deleteDataFromStorageServiceTest) testSuccessfulExecution(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToDelete).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbField).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataParameterID).
		Return(test.dataForTest.dataDbField).
		Once()

	test.mocks.dbCollection.
		On("DeleteOne", context.TODO(), test.dataForTest.bsonMForDeletion).
		Return(test.dataForTest.mongoDeleteResultWithCountOne, nil).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.successfulLogMessage, test.dataForTest.logDebugLevel).
		Once()

	actualError := test.deleteDataFromStorageService.executeDeletion(test.dataForTest.dataKey, test.dataForTest.dataToDelete)
	assert.Nil(t, actualError)
}

func (test *deleteDataFromStorageServiceTest) testFailureOnDeletionFromDb(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToDelete).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbField).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataParameterID).
		Return(test.dataForTest.dataDbField).
		Once()

	test.mocks.dbCollection.
		On("DeleteOne", context.TODO(), test.dataForTest.bsonMForDeletion).
		Return(nil, test.dataForTest.dbError).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.dbErrorLogMessage, test.dataForTest.logErrorLevel).
		Once()

	actualError := test.deleteDataFromStorageService.executeDeletion(test.dataForTest.dataKey, test.dataForTest.dataToDelete)
	assert.Error(t, actualError)
	assert.Equal(t, test.dataForTest.expectedErrorAfterDbFailure, actualError)
	assert.EqualError(t, actualError, test.dataForTest.expectedErrorAfterDbFailure.Error())
	assert.Error(t, actualError.Previous())
	assert.Equal(t, test.dataForTest.dbError, actualError.Previous())
}

func (test *deleteDataFromStorageServiceTest) testFailureOnNothingHasBeenDeleted(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToDelete).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyDbField).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataParameterID).
		Return(test.dataForTest.dataDbField).
		Once()

	test.mocks.dbCollection.
		On("DeleteOne", context.TODO(), test.dataForTest.bsonMForDeletion).
		Return(test.dataForTest.mongoDeleteResultWithCountZero, nil).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.nothingDeletedErrorLogMessage, test.dataForTest.logErrorLevel).
		Once()

	actualError := test.deleteDataFromStorageService.executeDeletion(test.dataForTest.dataKey, test.dataForTest.dataToDelete)
	assert.Error(t, actualError)
	assert.Equal(t, test.dataForTest.expectedErrorAfterNothingDeleted, actualError)
	assert.Error(t, actualError.Previous())
	assert.Equal(t, test.dataForTest.nothingDeletedError, actualError.Previous())
}

func (test *deleteDataFromStorageServiceTest) prepareDataForTest() {
	test.dataForTest.dataKey = "ThisIsMyKey1234567890"
	test.dataForTest.dataToDelete.value.name = "test name"
	test.dataForTest.dataToDelete.value.id = 1235
	test.dataForTest.dataToDelete.key = "MyInnerKey98765"
	test.dataForTest.dataKeyParameterID = newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	test.dataForTest.dataParameterID = newDataToStoreParameterIDWithPanicOnError(dataParameterID)
	test.dataForTest.dataKeyDbField = "dataKey"
	test.dataForTest.dataDbField = "data"
	test.dataForTest.bsonMForDeletion = bson.M{
		test.dataForTest.dataKeyDbField: test.dataForTest.dataKey,
		test.dataForTest.dataDbField:    test.dataForTest.dataToDelete,
	}
	test.dataForTest.mongoDeleteResultWithCountOne = new(mongo.DeleteResult)
	test.dataForTest.mongoDeleteResultWithCountOne.DeletedCount = 1
	test.dataForTest.mongoDeleteResultWithCountZero = new(mongo.DeleteResult)
	test.dataForTest.successfulLogMessage = "Data has been successfully deleted from storage"
	test.dataForTest.logDebugLevel = debugLevel
	test.dataForTest.logErrorLevel = errorLevel
	test.dataForTest.dbError = errors.New("some db error")
	test.dataForTest.expectedErrorAfterDbFailure = newFailedToDeleteDataFromStorageError(test.dataForTest.dbError)
	test.dataForTest.dbErrorLogMessage = fmt.Sprintf(
		"Failed to delete data from storage due to an error: %s",
		test.dataForTest.dbError,
	)
	test.dataForTest.nothingDeletedError = errors.New("nothing has been deleted")
	test.dataForTest.expectedErrorAfterNothingDeleted = newFailedToDeleteDataFromStorageError(
		test.dataForTest.nothingDeletedError,
	)
	test.dataForTest.nothingDeletedErrorLogMessage = fmt.Sprintf(
		"Failed to delete data from storage due to an error: %s",
		test.dataForTest.nothingDeletedError,
	)
}

func (test *deleteDataFromStorageServiceTest) prepareMocks() {
	test.mocks.getCollectionForDataService = new(mockGetCollectionForDataServiceInterface)
	test.mocks.getBsonParameterNameForDataToStoreDtoService = new(
		mockGetBsonParameterNameForDataToStoreDtoServiceInterface,
	)
	test.mocks.logger = new(mockLoggerServiceInterface)
	test.mocks.dbCollection = new(mockDbCollectionInterface)
}

type updateDataInStorageServiceTest struct {
	updateDataInStorageService updateDataInStorageServiceInterface
	dataForTest                struct {
		dataToStoreDto                  DataToStoreDto
		dataKeyParameterID              *dataToStoreParameterID
		dataKeyParameterName            string
		dataParameterID                 *dataToStoreParameterID
		dataParameterName               string
		expiresAtParameterID            *dataToStoreParameterID
		expiresAtParameterName          string
		updateFilter                    bson.M
		dataToUpdate                    bson.M
		mongoUpdateResultWithOneCount   *mongo.UpdateResult
		mongoUpdateResultWithZeroCount  *mongo.UpdateResult
		successfulLogMessage            string
		logDebugLevel                   int
		logErrorLevel                   int
		dbError                         error
		dbErrorLogMessage               string
		expectedErrorOnDbError          TraceableErrorInterface
		errorOnNothingUpdated           error
		errorOnNothingUpdatedLogMessage string
		expectedErrorOnNothingUpdated   TraceableErrorInterface
	}
	mocks struct {
		getCollectionForDataService                  *mockGetCollectionForDataServiceInterface
		getBsonParameterNameForDataToStoreDtoService *mockGetBsonParameterNameForDataToStoreDtoServiceInterface
		logger                                       *mockLoggerServiceInterface
		dbCollection                                 *mockDbCollectionInterface
	}
}

type testDataToStoreStruct struct {
	name string
	age  int
}

func TestUpdateDataInStorageService(t *testing.T) {
	test := &updateDataInStorageServiceTest{}
	test.prepareDataForTest()
	test.prepareMocks()
	test.updateDataInStorageService = newUpdateDataInStorageService(
		test.mocks.getCollectionForDataService,
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.mocks.logger,
	)

	t.Parallel()
	t.Run("testSuccessfulExecution", test.testSuccessfulExecution)
	t.Run("testFailureOnErrorFromDb", test.testFailureOnErrorFromDb)
	t.Run("testFailureOnNothingHasBeenUpdated", test.testFailureOnNothingHasBeenUpdated)
}

func (test *updateDataInStorageServiceTest) testSuccessfulExecution(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToStoreDto.Data).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyParameterName).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataParameterID).
		Return(test.dataForTest.dataParameterName).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.expiresAtParameterID).
		Return(test.dataForTest.expiresAtParameterName).
		Once()

	test.mocks.dbCollection.
		On("UpdateOne", context.TODO(), test.dataForTest.updateFilter, test.dataForTest.dataToUpdate).
		Return(test.dataForTest.mongoUpdateResultWithOneCount, nil).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.successfulLogMessage, test.dataForTest.logDebugLevel).
		Once()

	err := test.updateDataInStorageService.executeUpdating(test.dataForTest.dataToStoreDto)
	assert.Nil(t, err)
}

func (test *updateDataInStorageServiceTest) testFailureOnErrorFromDb(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToStoreDto.Data).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyParameterName).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataParameterID).
		Return(test.dataForTest.dataParameterName).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.expiresAtParameterID).
		Return(test.dataForTest.expiresAtParameterName).
		Once()

	test.mocks.dbCollection.
		On("UpdateOne", context.TODO(), test.dataForTest.updateFilter, test.dataForTest.dataToUpdate).
		Return(nil, test.dataForTest.dbError).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.dbErrorLogMessage, test.dataForTest.logErrorLevel).
		Once()

	err := test.updateDataInStorageService.executeUpdating(test.dataForTest.dataToStoreDto)
	assert.Error(t, err)
	assert.Equal(t, test.dataForTest.expectedErrorOnDbError, err)
	assert.EqualError(t, err, test.dataForTest.expectedErrorOnDbError.Error())
	assert.Error(t, err.Previous())
	assert.Equal(t, test.dataForTest.dbError, err.Previous())
}

func (test *updateDataInStorageServiceTest) testFailureOnNothingHasBeenUpdated(t *testing.T) {
	test.mocks.getCollectionForDataService.
		On("execute", test.dataForTest.dataToStoreDto.Data).
		Return(test.mocks.dbCollection).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataKeyParameterID).
		Return(test.dataForTest.dataKeyParameterName).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.dataParameterID).
		Return(test.dataForTest.dataParameterName).
		Once()

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.expiresAtParameterID).
		Return(test.dataForTest.expiresAtParameterName).
		Once()

	test.mocks.dbCollection.
		On("UpdateOne", context.TODO(), test.dataForTest.updateFilter, test.dataForTest.dataToUpdate).
		Return(test.dataForTest.mongoUpdateResultWithZeroCount, nil).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.errorOnNothingUpdatedLogMessage, test.dataForTest.logErrorLevel).
		Once()

	err := test.updateDataInStorageService.executeUpdating(test.dataForTest.dataToStoreDto)
	assert.Error(t, err)
	assert.Equal(t, test.dataForTest.expectedErrorOnNothingUpdated, err)
	assert.Error(t, err.Previous())
	assert.Equal(t, test.dataForTest.errorOnNothingUpdated, err.Previous())
}

func (test *updateDataInStorageServiceTest) prepareDataForTest() {
	test.dataForTest.dataToStoreDto = DataToStoreDto{
		DataKey: "testDataKey",
		Data: testDataToStoreStruct{
			name: "Matthew Murdock",
			age:  28,
		},
		ExpiresAt: time.Date(2020, 1, 1, 0, 0, 0, 0, time.UTC),
	}
	test.dataForTest.dataKeyParameterID = newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	test.dataForTest.dataKeyParameterName = "dataKey"
	test.dataForTest.dataParameterID = newDataToStoreParameterIDWithPanicOnError(dataParameterID)
	test.dataForTest.dataParameterName = "data"
	test.dataForTest.expiresAtParameterID = newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID)
	test.dataForTest.expiresAtParameterName = "expiresAt"
	test.dataForTest.updateFilter = bson.M{
		test.dataForTest.dataKeyParameterName: test.dataForTest.dataToStoreDto.DataKey,
	}
	test.dataForTest.dataToUpdate = bson.M{
		"$set": bson.M{
			test.dataForTest.dataParameterName:      test.dataForTest.dataToStoreDto.Data,
			test.dataForTest.expiresAtParameterName: test.dataForTest.dataToStoreDto.ExpiresAt,
		},
	}
	test.dataForTest.mongoUpdateResultWithOneCount = new(mongo.UpdateResult)
	test.dataForTest.mongoUpdateResultWithOneCount.ModifiedCount = 1
	test.dataForTest.mongoUpdateResultWithZeroCount = new(mongo.UpdateResult)
	test.dataForTest.successfulLogMessage = "Data in storage has been successfully updated"
	test.dataForTest.logDebugLevel = debugLevel
	test.dataForTest.logErrorLevel = errorLevel
	test.dataForTest.dbError = errors.New("some error from DB")
	test.dataForTest.dbErrorLogMessage = "Failed to update data in storage due to an error: some error from DB"
	test.dataForTest.expectedErrorOnDbError = newFailedToUpdateDataInStorageError(test.dataForTest.dbError)
	test.dataForTest.errorOnNothingUpdated = errors.New("nothing has been updated")
	test.dataForTest.errorOnNothingUpdatedLogMessage = "Failed to update data in storage due to an error: " +
		"nothing has been updated"
	test.dataForTest.expectedErrorOnNothingUpdated = newFailedToUpdateDataInStorageError(
		test.dataForTest.errorOnNothingUpdated,
	)
}

func (test *updateDataInStorageServiceTest) prepareMocks() {
	test.mocks.getBsonParameterNameForDataToStoreDtoService = new(
		mockGetBsonParameterNameForDataToStoreDtoServiceInterface,
	)
	test.mocks.getCollectionForDataService = new(mockGetCollectionForDataServiceInterface)
	test.mocks.logger = new(mockLoggerServiceInterface)
	test.mocks.dbCollection = new(mockDbCollectionInterface)
}
