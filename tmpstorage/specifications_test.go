package tmpstorage

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

type dataKeyDbIndexIsDtoIsValidSpecificationTest struct {
	dataKeyDbIndexIsDtoIsValidSpecification dataKeyDbIndexIsDtoIsValidSpecificationInterface
	dataForTest                             struct {
		correctDto        dataKeyDbIndexDto
		incorrectDtoSlice []dataKeyDbIndexDto
	}
}

func TestDataKeyDbIndexIsDtoIsValidSpecification(t *testing.T) {
	test := &dataKeyDbIndexIsDtoIsValidSpecificationTest{}
	test.dataKeyDbIndexIsDtoIsValidSpecification = newDataKeyDbIndexIsDtoIsValidSpecification()
	test.prepareDataForTest()

	t.Parallel()
	t.Run("testSuccessfulPositiveCheck", test.testSuccessfulPositiveCheck)
	t.Run("testSuccessfulNegativeCheck", test.testSuccessfulNegativeCheck)

}

func (test *dataKeyDbIndexIsDtoIsValidSpecificationTest) testSuccessfulPositiveCheck(t *testing.T) {
	actualResult := test.dataKeyDbIndexIsDtoIsValidSpecification.isSatisfiedBy(test.dataForTest.correctDto)
	assert.True(t, actualResult)
}

func (test *dataKeyDbIndexIsDtoIsValidSpecificationTest) testSuccessfulNegativeCheck(t *testing.T) {
	for _, dto := range test.dataForTest.incorrectDtoSlice {
		actualResult := test.dataKeyDbIndexIsDtoIsValidSpecification.isSatisfiedBy(dto)
		assert.False(t, actualResult)
	}
}

func (test *dataKeyDbIndexIsDtoIsValidSpecificationTest) prepareDataForTest() {
	test.dataForTest.incorrectDtoSlice = []dataKeyDbIndexDto{
		{
			Version: 0,
			Unique:  false,
			Key: struct {
				DataKey int
			}{DataKey: 0},
			Name:      "incorrect key and not unique",
			Namespace: "some namespace",
		},
		{
			Version: 0,
			Unique:  true,
			Key: struct {
				DataKey int
			}{DataKey: 0},
			Name:      "incorrect key and unique",
			Namespace: "some namespace",
		},
		{
			Version: 0,
			Unique:  false,
			Key: struct {
				DataKey int
			}{DataKey: 1},
			Name:      "correct key and not unique",
			Namespace: "some namespace",
		},
	}
	test.dataForTest.correctDto = dataKeyDbIndexDto{
		Version: 0,
		Unique:  true,
		Key: struct {
			DataKey int
		}{DataKey: 1},
		Name:      "correct key",
		Namespace: "some namespace",
	}
}

type expiresAtDbIndexDtoIsValidSpecificationTest struct {
	expiresAtDbIndexDtoIsValidSpecification expiresAtDbIndexDtoIsValidSpecificationInterface
	dataForTest                             struct {
		correctDto   expiresAtDbIndexDto
		incorrectDto expiresAtDbIndexDto
	}
}

func TestExpiresAtDbIndexDtoIsValidSpecification(t *testing.T) {
	test := expiresAtDbIndexDtoIsValidSpecificationTest{}
	test.prepareDataForTest()
	test.expiresAtDbIndexDtoIsValidSpecification = newExpiresAtDbIndexDtoIsValidSpecification()

	t.Parallel()
	t.Run("testSuccessfulPositiveCheck", test.testSuccessfulPositiveCheck)
	t.Run("testSuccessfulNegativeCheck", test.testSuccessfulNegativeCheck)
}

func (test *expiresAtDbIndexDtoIsValidSpecificationTest) testSuccessfulPositiveCheck(t *testing.T) {
	actualResult := test.expiresAtDbIndexDtoIsValidSpecification.isSatisfiedBy(test.dataForTest.correctDto)
	assert.True(t, actualResult)
}

func (test *expiresAtDbIndexDtoIsValidSpecificationTest) testSuccessfulNegativeCheck(t *testing.T) {
	actualResult := test.expiresAtDbIndexDtoIsValidSpecification.isSatisfiedBy(test.dataForTest.incorrectDto)
	assert.False(t, actualResult)
}

func (test *expiresAtDbIndexDtoIsValidSpecificationTest) prepareDataForTest() {
	test.dataForTest.correctDto = expiresAtDbIndexDto{
		Version: 0,
		Key: struct {
			ExpiresAt int
		}{ExpiresAt: 1},
		Name:               "correct dto",
		Namespace:          "test namespace",
		ExpireAfterSeconds: 0,
	}
	test.dataForTest.incorrectDto = expiresAtDbIndexDto{
		Version: 0,
		Key: struct {
			ExpiresAt int
		}{ExpiresAt: 0},
		Name:               "incorrect dto",
		Namespace:          "test namespace",
		ExpireAfterSeconds: 0,
	}
}
