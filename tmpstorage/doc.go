// Package tmpstorage is a temporary data storage based on using mongo DB
//
// Data of any type can be stored for indicated period of time.
// Mongodb set up is required for this package. Visit https://docs.mongodb.com/ to
// find assistance with accomplishing following steps:
//
// 1. set up mongodb
//
// 2. create user
//
// 3. create database for data storage
//
// Usage starts with DataStorage initialization, using DB URL, DB name and log switcher:
//		storage, err := tmpstorage.InitializeDataStorage(
//			"mongodb://username:password@localhost:27017",
//			"my_database",
//			false,
//		)
//
//		if err != nil {
//			log.Panic(err)
//		}
//
// This will create storage instance, which is responsible for adding, updating,
// loading and deleting data
//
// Data format for adding and updating is specific and can be created via factory,
// which is injected. So if you want to add some Person type data to storage:
//		type Person struct {
//			name string
//			age int
//		}
//
// You should do the following:
//		person := Person{name: "Mathew Murdock", age: 28}
// 		dataToStoreDto := storage.DataToStoreDtoFactory.CreateWithExistenceDuration(
//			"thisIsMyDataKey",
//			person,
//			1000*time.Second, //TTL duration from now
//		)
//
// Considering all the above mentioned, data can be added as follows:
// 		err := storage.Add(dataToStoreDto)
//		if err != nil {
//			log.Panic(err)
//		}
//
// And updates as follows:
//		err := storage.Update(dataToStoreDto)
//		if err != nil {
//			log.Panic(err)
//		}
//
// Loaded from storage as follows:
//		loadedPerson := new(Person)
//		//person will be filled with data from storage
//		err := storage.Load("dataKeyToFind", loadedPerson)
//		if err != nil {
//			log.Panic(err)
//		}
//
// Deleted from storage as follows:
//		err := storage.Delete("dataKeyToDelete", person)
//		if err != nil {
//			log.Panic(err)
//		}
//
package tmpstorage
