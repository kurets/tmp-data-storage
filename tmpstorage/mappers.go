package tmpstorage

import "go.mongodb.org/mongo-driver/bson"

type dataToStoreDtoToBsonDMapperInterface interface {
	executeMapping(dataToStoreDto DataToStoreDto, bsonD *bson.D) TraceableErrorInterface
}

type dataToStoreDtoToBsonDMapper struct {
	marshalerService marshalerServiceInterface
}

func newDataToStoreDtoToBsonDMapper(
	marshalerService marshalerServiceInterface,
) *dataToStoreDtoToBsonDMapper {
	return &dataToStoreDtoToBsonDMapper{
		marshalerService: marshalerService,
	}
}

func (mapper *dataToStoreDtoToBsonDMapper) executeMapping(
	dataToStoreDto DataToStoreDto,
	bsonD *bson.D,
) TraceableErrorInterface {
	dataToStoreDtoBytes, err := mapper.marshalerService.marshalBSON(dataToStoreDto)
	if err != nil {
		return newFailedToMapDataToStoreDtoToBson(err)
	}

	err = mapper.marshalerService.unmarshalBSON(dataToStoreDtoBytes, bsonD)
	if err != nil {
		return newFailedToMapDataToStoreDtoToBson(err)
	}

	return nil
}

type dbFindOneResultToBsonDMapperInterface interface {
	executeMapping(resultInterface dbFindOneResultInterface, bsonD *bson.D) error
}

type dbFindOneResultToBsonDMapper struct{}

func newDbFindOneResultToBsonDMapper() *dbFindOneResultToBsonDMapper {
	return &dbFindOneResultToBsonDMapper{}
}

func (mapper *dbFindOneResultToBsonDMapper) executeMapping(
	resultInterface dbFindOneResultInterface,
	bsonD *bson.D,
) error {
	return resultInterface.Decode(bsonD)
}
