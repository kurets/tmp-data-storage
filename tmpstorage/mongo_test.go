package tmpstorage

import (
	"context"
	"errors"
	"fmt"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"testing"
	"time"
)

type connectToMongoDbServiceTest struct {
	dataForTest struct {
		dbURL                    dbURL
		dbName                   dbName
		expectedDbConnectorError error
		dbConnectorWithError     dbConnector
		dbConnector              dbConnector
		expectedLogMessage       string
		expectedDb               *mongo.Database
	}
	mocks struct {
		db           *mockDbInterface
		dbConnection *mockDbConnectionInterface
	}
}

func TestConnectToMongoDbServiceTest(t *testing.T) {
	test := new(connectToMongoDbServiceTest)
	test.prepareDataForTest()
	test.prepareMocks()

	t.Parallel()
	t.Run("testSuccessfulConnecting", test.testSuccessfulConnecting)
	t.Run("testFailsOnDbConnectorError", test.testFailsOnDbConnectorError)
	t.Run("testDatabase", test.testDatabase)
}

func (test *connectToMongoDbServiceTest) testSuccessfulConnecting(t *testing.T) {
	connectToMongoDbService := newConnectToMongoDbService(
		test.dataForTest.dbConnector,
		test.dataForTest.dbURL,
		test.dataForTest.dbName,
	)

	actualDbConnection, err := connectToMongoDbService.connect()
	assert.NotNil(t, actualDbConnection)
	assert.Nil(t, err)
}

func (test *connectToMongoDbServiceTest) testFailsOnDbConnectorError(t *testing.T) {
	connectToMongoDbService := newConnectToMongoDbService(
		test.dataForTest.dbConnectorWithError,
		test.dataForTest.dbURL,
		test.dataForTest.dbName,
	)

	dbConnection, err := connectToMongoDbService.connect()
	assert.Nil(t, dbConnection)
	assert.Error(t, err)
	assert.EqualError(t, err, test.dataForTest.expectedDbConnectorError.Error())
}

func (test *connectToMongoDbServiceTest) testDatabase(t *testing.T) {
	connectToMongoDbService := newConnectToMongoDbService(
		test.dataForTest.dbConnector,
		test.dataForTest.dbURL,
		test.dataForTest.dbName,
	)

	test.mocks.dbConnection.
		On("Database", string(test.dataForTest.dbName)).
		Return(test.dataForTest.expectedDb).
		Once()

	actualDb := connectToMongoDbService.database(test.mocks.dbConnection)

	assert.Equal(t, test.dataForTest.expectedDb, actualDb)
}

func (test *connectToMongoDbServiceTest) prepareDataForTest() {
	test.dataForTest.dbURL = "some/db/url"
	test.dataForTest.dbName = "database_name"
	test.dataForTest.dbConnector = func(
		ctx context.Context,
		opts ...*options.ClientOptions,
	) (client *mongo.Client, err error) {
		return new(mongo.Client), nil
	}

	test.dataForTest.expectedDbConnectorError = errors.New("failed to establish connection")
	test.dataForTest.dbConnectorWithError = func(
		ctx context.Context,
		opts ...*options.ClientOptions,
	) (client *mongo.Client, err error) {
		return nil, test.dataForTest.expectedDbConnectorError
	}

	timeNowStringWithoutSeconds := time.Now().Format("2006/01/02 15:04")
	test.dataForTest.expectedLogMessage = fmt.Sprintf(
		"%s:[0-9]{2} %s",
		timeNowStringWithoutSeconds,
		`Connected to storage database`,
	)

	test.dataForTest.expectedDb = new(mongo.Database)
}

func (test *connectToMongoDbServiceTest) prepareMocks() {
	test.mocks.db = new(mockDbInterface)
	test.mocks.dbConnection = new(mockDbConnectionInterface)
}

type createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest struct {
	createUniqueIndexService *createUniqueIndexForDataKeyInCollectionIfNotExistsService
	dataForTest              struct {
		supportedParameterID                     *dataToStoreParameterID
		unsupportedParameterID                   *dataToStoreParameterID
		parameterIDIsNotSupportedError           *parameterIDIsNotSupportedError
		dataKeyDbIndexField                      string
		dataKeyDbIndexDtoSliceWithCorrectIndex   []dataKeyDbIndexDto
		dataKeyDbIndexDtoSliceWithIncorrectIndex []dataKeyDbIndexDto
		uniqueKeyIndex                           mongo.IndexModel
		collectionName                           string
		logMessage                               string
		logLevel                                 int
	}
	mocks struct {
		getBsonParameterNameForDataToStoreDtoService *mockGetBsonParameterNameForDataToStoreDtoServiceInterface
		logger                                       *mockLoggerServiceInterface
		collection                                   *mockDbCollectionInterface
		dbIndexManagerService                        *mockDbIndexManagerServiceInterface
		dataKeyDbIndexIsDtoIsValidSpecification      *mockDataKeyDbIndexIsDtoIsValidSpecificationInterface
		dbIterator                                   *mockDbIteratorInterface
		dbCollectionIndexView                        *mockDbCollectionIndexViewInterface
	}
}

func TestCreateUniqueIndexForDataKeyInCollectionIfNotExistsService(t *testing.T) {
	test := new(createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest)
	test.prepareDataForTest()
	test.prepareMocks()
	test.createUniqueIndexService, _ = newCreateUniqueIndexForDataKeyInCollectionIfNotExistsService(
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.dataForTest.supportedParameterID,
		test.mocks.dbIndexManagerService,
		test.mocks.dataKeyDbIndexIsDtoIsValidSpecification,
		test.mocks.logger,
	)

	t.Parallel()
	t.Run("testSuccessfulServiceConstruction", test.testSuccessfulServiceConstruction)
	t.Run("testServiceConstructionFailure", test.testServiceConstructionFailure)
	t.Run("testSuccessfulExecutionWithIndexExists", test.testSuccessfulExecutionWithIndexExists)
	t.Run("testSuccessfulExecutionIndexCreation", test.testSuccessfulExecutionIndexCreation)
}

func (test *createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest) testSuccessfulServiceConstruction(
	t *testing.T,
) {
	service, err := newCreateUniqueIndexForDataKeyInCollectionIfNotExistsService(
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.dataForTest.supportedParameterID,
		test.mocks.dbIndexManagerService,
		test.mocks.dataKeyDbIndexIsDtoIsValidSpecification,
		test.mocks.logger,
	)

	assert.Nil(t, err)
	assert.NotNil(t, service)
	assert.IsType(t, &createUniqueIndexForDataKeyInCollectionIfNotExistsService{}, service)
	assert.Equal(t, test.dataForTest.supportedParameterID, service.parameterID)
}

func (test *createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest) testServiceConstructionFailure(
	t *testing.T,
) {
	service, err := newCreateUniqueIndexForDataKeyInCollectionIfNotExistsService(
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.dataForTest.unsupportedParameterID,
		test.mocks.dbIndexManagerService,
		test.mocks.dataKeyDbIndexIsDtoIsValidSpecification,
		test.mocks.logger,
	)

	assert.Nil(t, service)
	assert.IsType(t, &createUniqueIndexForDataKeyInCollectionIfNotExistsService{}, service)
	assert.Error(t, err)
	assert.EqualError(t, err, test.dataForTest.parameterIDIsNotSupportedError.Error())
	assert.Equal(t, err, test.dataForTest.parameterIDIsNotSupportedError)
}

func (test *createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest) testSuccessfulExecutionWithIndexExists(
	t *testing.T,
) {
	collection := test.mocks.collection
	test.mocks.dbIndexManagerService.
		On("retrieveCollectionIndexes", collection).
		Return(test.mocks.dbCollectionIndexView).
		Once()

	test.mocks.dbIndexManagerService.
		On("indexesIterator", test.mocks.dbCollectionIndexView).
		Return(test.mocks.dbIterator).
		Once()

	test.mocks.dbIndexManagerService.
		On("mapIndexesToStructSlice", test.mocks.dbIterator, new([]dataKeyDbIndexDto)).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*[]dataKeyDbIndexDto)
			*arg = test.dataForTest.dataKeyDbIndexDtoSliceWithCorrectIndex
		})

	test.mocks.dataKeyDbIndexIsDtoIsValidSpecification.
		On("isSatisfiedBy", test.dataForTest.dataKeyDbIndexDtoSliceWithCorrectIndex[0]).
		Return(false).
		Once()

	test.mocks.dataKeyDbIndexIsDtoIsValidSpecification.
		On("isSatisfiedBy", test.dataForTest.dataKeyDbIndexDtoSliceWithCorrectIndex[1]).
		Return(true).
		Once()

	test.mocks.dataKeyDbIndexIsDtoIsValidSpecification.
		AssertNotCalled(t, "isSatisfiedBy", test.dataForTest.dataKeyDbIndexDtoSliceWithCorrectIndex[2])

	test.createUniqueIndexService.execute(collection)
}

func (test *createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest) testSuccessfulExecutionIndexCreation(
	_ *testing.T,
) {
	collection := test.mocks.collection
	test.mocks.dbIndexManagerService.
		On("retrieveCollectionIndexes", collection).
		Return(test.mocks.dbCollectionIndexView).
		Once()

	test.mocks.dbIndexManagerService.
		On("indexesIterator", test.mocks.dbCollectionIndexView).
		Return(test.mocks.dbIterator).
		Once()

	test.mocks.dbIndexManagerService.
		On("mapIndexesToStructSlice", test.mocks.dbIterator, new([]dataKeyDbIndexDto)).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*[]dataKeyDbIndexDto)
			*arg = test.dataForTest.dataKeyDbIndexDtoSliceWithIncorrectIndex
		})

	for _, incorrectIndexDto := range test.dataForTest.dataKeyDbIndexDtoSliceWithIncorrectIndex {
		test.mocks.dataKeyDbIndexIsDtoIsValidSpecification.
			On("isSatisfiedBy", incorrectIndexDto).
			Return(false).
			Once()
	}

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.supportedParameterID).
		Return(test.dataForTest.dataKeyDbIndexField, nil).
		Once()

	test.mocks.dbIndexManagerService.
		On("createIndex", test.mocks.dbCollectionIndexView, test.dataForTest.uniqueKeyIndex).
		Once()

	collection.
		On("Name").
		Return(test.dataForTest.collectionName).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.logMessage, test.dataForTest.logLevel).
		Once()

	test.createUniqueIndexService.execute(collection)
}

func (test *createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest) prepareDataForTest() {
	test.dataForTest.supportedParameterID = newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	test.dataForTest.unsupportedParameterID = newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID)
	test.dataForTest.parameterIDIsNotSupportedError = newParameterIDIsNotSupportedError(expiresAtParameterID)
	test.dataForTest.dataKeyDbIndexField = "dataKey"
	test.dataForTest.dataKeyDbIndexDtoSliceWithCorrectIndex = []dataKeyDbIndexDto{
		{
			Version: 0,
			Unique:  false,
			Key: struct {
				DataKey int
			}{DataKey: 0},
			Name:      "wrong index name and not unique",
			Namespace: "someNamespace",
		},
		{
			Version: 0,
			Unique:  false,
			Key: struct {
				DataKey int
			}{DataKey: 1},
			Name:      "correct index name, but not unique",
			Namespace: "someNamespace",
		},
		{
			Version: 0,
			Unique:  true,
			Key: struct {
				DataKey int
			}{DataKey: 0},
			Name:      "wrong index name, but unique",
			Namespace: "someNamespace",
		},
		{
			Version: 0,
			Unique:  true,
			Key: struct {
				DataKey int
			}{DataKey: 1},
			Name:      "correct index",
			Namespace: "someNamespace",
		},
		{
			Version: 0,
			Unique:  false,
			Key: struct {
				DataKey int
			}{DataKey: 0},
			Name:      "another wrong index name and not unique",
			Namespace: "someNamespace",
		},
	}

	test.dataForTest.dataKeyDbIndexDtoSliceWithIncorrectIndex = append(
		test.dataForTest.dataKeyDbIndexDtoSliceWithCorrectIndex[:2],
		test.dataForTest.dataKeyDbIndexDtoSliceWithCorrectIndex[3:]...,
	)

	uniqueKeyIndexOptions := options.IndexOptions{}
	uniqueKeyIndexOptions.SetUnique(true)
	test.dataForTest.uniqueKeyIndex = mongo.IndexModel{
		Keys:    bson.M{test.dataForTest.dataKeyDbIndexField: 1},
		Options: &uniqueKeyIndexOptions,
	}
	test.dataForTest.collectionName = "collectionName"
	test.dataForTest.logMessage = fmt.Sprintf(
		"Index for field '%s' in collection '%s' has been created",
		test.dataForTest.dataKeyDbIndexField,
		test.dataForTest.collectionName,
	)
	test.dataForTest.logLevel = debugLevel
}

func (test *createUniqueIndexForDataKeyInCollectionIfNotExistsServiceTest) prepareMocks() {
	test.mocks.getBsonParameterNameForDataToStoreDtoService = new(
		mockGetBsonParameterNameForDataToStoreDtoServiceInterface,
	)
	test.mocks.logger = new(mockLoggerServiceInterface)
	test.mocks.collection = new(mockDbCollectionInterface)
	test.mocks.dbIndexManagerService = new(mockDbIndexManagerServiceInterface)
	test.mocks.dataKeyDbIndexIsDtoIsValidSpecification = new(mockDataKeyDbIndexIsDtoIsValidSpecificationInterface)
	test.mocks.dbIterator = new(mockDbIteratorInterface)
	test.mocks.dbCollectionIndexView = new(mockDbCollectionIndexViewInterface)
}

type createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest struct {
	createTTLIndexForExpiresAtService *createTTLIndexForExpiresAtInCollectionIfNotExistsService
	dataForTest                       struct {
		supportedParameterID                       *dataToStoreParameterID
		unsupportedParameterID                     *dataToStoreParameterID
		parameterIDIsNotSupportedError             *parameterIDIsNotSupportedError
		expiresAtDbIndexField                      string
		expiresAtDbIndexDtoSliceWithCorrectIndex   []expiresAtDbIndexDto
		expiresAtDbIndexDtoSliceWithIncorrectIndex []expiresAtDbIndexDto
		expireAfterSecondsIndex                    mongo.IndexModel
		collectionName                             string
		logMessage                                 string
		logLevel                                   int
	}
	mocks struct {
		getBsonParameterNameForDataToStoreDtoService *mockGetBsonParameterNameForDataToStoreDtoServiceInterface
		logger                                       *mockLoggerServiceInterface
		collection                                   *mockDbCollectionInterface
		dbIndexManagerService                        *mockDbIndexManagerServiceInterface
		expiresAtDbIndexDtoIsValidSpecification      *mockExpiresAtDbIndexDtoIsValidSpecificationInterface
		dbIterator                                   *mockDbIteratorInterface
		dbCollectionIndexView                        *mockDbCollectionIndexViewInterface
	}
}

func TestCreateTTLIndexForExpiresAtInCollectionIfNotExistsService(t *testing.T) {
	test := new(createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest)
	test.prepareMocks()
	test.prepareDataForTest()
	test.createTTLIndexForExpiresAtService, _ = newCreateTTLIndexForExpiresAtInCollectionIfNotExistsService(
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.dataForTest.supportedParameterID,
		test.mocks.dbIndexManagerService,
		test.mocks.expiresAtDbIndexDtoIsValidSpecification,
		test.mocks.logger,
	)

	t.Parallel()
	t.Run("testSuccessfulServiceConstruction", test.testSuccessfulServiceConstruction)
	t.Run("testServiceConstructionFailure", test.testServiceConstructionFailure)
	t.Run("testSuccessfulExecutionIndexCreation", test.testSuccessfulExecutionWithIndexAlreadyExists)
	t.Run("testSuccessfulExecutionIndexCreation", test.testSuccessfulExecutionIndexCreation)
}

func (test *createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest) testSuccessfulServiceConstruction(
	t *testing.T,
) {
	service, err := newCreateTTLIndexForExpiresAtInCollectionIfNotExistsService(
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.dataForTest.supportedParameterID,
		test.mocks.dbIndexManagerService,
		test.mocks.expiresAtDbIndexDtoIsValidSpecification,
		test.mocks.logger,
	)

	assert.Nil(t, err)
	assert.NotNil(t, service)
	assert.IsType(t, &createTTLIndexForExpiresAtInCollectionIfNotExistsService{}, service)
	assert.Equal(t, test.dataForTest.supportedParameterID, service.parameterID)
}

func (test *createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest) testServiceConstructionFailure(
	t *testing.T,
) {
	service, err := newCreateTTLIndexForExpiresAtInCollectionIfNotExistsService(
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.dataForTest.unsupportedParameterID,
		test.mocks.dbIndexManagerService,
		test.mocks.expiresAtDbIndexDtoIsValidSpecification,
		test.mocks.logger,
	)

	assert.Nil(t, service)
	assert.IsType(t, &createTTLIndexForExpiresAtInCollectionIfNotExistsService{}, service)
	assert.Error(t, err)
	assert.EqualError(t, err, test.dataForTest.parameterIDIsNotSupportedError.Error())
	assert.Equal(t, err, test.dataForTest.parameterIDIsNotSupportedError)
}

func (test *createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest) testSuccessfulExecutionWithIndexAlreadyExists(
	t *testing.T,
) {
	collection := test.mocks.collection
	test.mocks.dbIndexManagerService.
		On("retrieveCollectionIndexes", collection).
		Return(test.mocks.dbCollectionIndexView).
		Once()

	test.mocks.dbIndexManagerService.
		On("indexesIterator", test.mocks.dbCollectionIndexView).
		Return(test.mocks.dbIterator).
		Once()

	test.mocks.dbIndexManagerService.
		On("mapIndexesToStructSlice", test.mocks.dbIterator, new([]expiresAtDbIndexDto)).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*[]expiresAtDbIndexDto)
			*arg = test.dataForTest.expiresAtDbIndexDtoSliceWithCorrectIndex
		})

	test.mocks.expiresAtDbIndexDtoIsValidSpecification.
		On("isSatisfiedBy", test.dataForTest.expiresAtDbIndexDtoSliceWithCorrectIndex[0]).
		Return(false).
		Once()

	test.mocks.expiresAtDbIndexDtoIsValidSpecification.
		On("isSatisfiedBy", test.dataForTest.expiresAtDbIndexDtoSliceWithCorrectIndex[1]).
		Return(true).
		Once()

	test.mocks.expiresAtDbIndexDtoIsValidSpecification.
		AssertNotCalled(t, "isSatisfiedBy", test.dataForTest.expiresAtDbIndexDtoSliceWithCorrectIndex[2])

	test.createTTLIndexForExpiresAtService.execute(collection)
}

func (test *createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest) testSuccessfulExecutionIndexCreation(
	_ *testing.T,
) {
	collection := test.mocks.collection
	test.mocks.dbIndexManagerService.
		On("retrieveCollectionIndexes", collection).
		Return(test.mocks.dbCollectionIndexView).
		Once()

	test.mocks.dbIndexManagerService.
		On("indexesIterator", test.mocks.dbCollectionIndexView).
		Return(test.mocks.dbIterator).
		Once()

	test.mocks.dbIndexManagerService.
		On("mapIndexesToStructSlice", test.mocks.dbIterator, new([]expiresAtDbIndexDto)).
		Once().
		Run(func(args mock.Arguments) {
			arg := args.Get(1).(*[]expiresAtDbIndexDto)
			*arg = test.dataForTest.expiresAtDbIndexDtoSliceWithIncorrectIndex
		})

	for _, incorrectIndexDto := range test.dataForTest.expiresAtDbIndexDtoSliceWithIncorrectIndex {
		test.mocks.expiresAtDbIndexDtoIsValidSpecification.
			On("isSatisfiedBy", incorrectIndexDto).
			Return(false).
			Once()
	}

	test.mocks.getBsonParameterNameForDataToStoreDtoService.
		On("execute", test.dataForTest.supportedParameterID).
		Return(test.dataForTest.expiresAtDbIndexField, nil).
		Once()

	test.mocks.dbIndexManagerService.
		On("createIndex", test.mocks.dbCollectionIndexView, test.dataForTest.expireAfterSecondsIndex).
		Once()

	collection.
		On("Name").
		Return(test.dataForTest.collectionName).
		Once()

	test.mocks.logger.
		On("log", test.dataForTest.logMessage, test.dataForTest.logLevel).
		Once()

	test.createTTLIndexForExpiresAtService.execute(collection)
}

func (test *createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest) prepareDataForTest() {
	test.dataForTest.supportedParameterID = newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID)
	test.dataForTest.unsupportedParameterID = newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	test.dataForTest.parameterIDIsNotSupportedError = newParameterIDIsNotSupportedError(dataKeyParameterID)
	test.dataForTest.expiresAtDbIndexField = "expiresAt"
	test.dataForTest.expiresAtDbIndexDtoSliceWithCorrectIndex = []expiresAtDbIndexDto{
		{
			Version: 0,
			Key: struct {
				ExpiresAt int
			}{ExpiresAt: 0},
			Name:               "wrong index",
			Namespace:          "someNamespace",
			ExpireAfterSeconds: 0,
		},
		{
			Version: 0,
			Key: struct {
				ExpiresAt int
			}{ExpiresAt: 1},
			Name:               "correct index",
			Namespace:          "someNamespace",
			ExpireAfterSeconds: 0,
		},
		{
			Version: 0,
			Key: struct {
				ExpiresAt int
			}{ExpiresAt: 0},
			Name:               "another wrong index",
			Namespace:          "someNamespace",
			ExpireAfterSeconds: 0,
		},
	}
	test.dataForTest.expiresAtDbIndexDtoSliceWithIncorrectIndex = test.dataForTest.
		expiresAtDbIndexDtoSliceWithCorrectIndex

	test.dataForTest.expiresAtDbIndexDtoSliceWithIncorrectIndex[1].Key.ExpiresAt = 0

	expireAfterSecondsIndexOptions := options.IndexOptions{}
	expireAfterSecondsIndexOptions.SetExpireAfterSeconds(0)
	test.dataForTest.expireAfterSecondsIndex = mongo.IndexModel{
		Keys:    bson.M{test.dataForTest.expiresAtDbIndexField: 1},
		Options: &expireAfterSecondsIndexOptions,
	}
	test.dataForTest.collectionName = "collectionName"
	test.dataForTest.logMessage = fmt.Sprintf(
		"Index for field '%s' in collection '%s' has been created",
		test.dataForTest.expiresAtDbIndexField,
		test.dataForTest.collectionName,
	)
	test.dataForTest.logLevel = debugLevel
}

func (test *createTTLIndexForExpiresAtInCollectionIfNotExistsServiceTest) prepareMocks() {
	test.mocks.getBsonParameterNameForDataToStoreDtoService = new(
		mockGetBsonParameterNameForDataToStoreDtoServiceInterface,
	)
	test.mocks.logger = new(mockLoggerServiceInterface)
	test.mocks.collection = new(mockDbCollectionInterface)
	test.mocks.dbIndexManagerService = new(mockDbIndexManagerServiceInterface)
	test.mocks.expiresAtDbIndexDtoIsValidSpecification = new(mockExpiresAtDbIndexDtoIsValidSpecificationInterface)
	test.mocks.dbIterator = new(mockDbIteratorInterface)
	test.mocks.dbCollectionIndexView = new(mockDbCollectionIndexViewInterface)
}

type dbIndexManagerServiceTest struct {
	dbIndexManagerService dbIndexManagerServiceInterface
	dataForTest           struct {
		expectedDtoSlice []struct {
			firstParameter  string
			secondParameter int
		}
		emptyDtoSlice []struct {
			firstParameter  string
			secondParameter int
		}
		mongoIndexView     mongo.IndexView
		mongoCursor        *mongo.Cursor
		expectedError      error
		expectedPanicValue string
		mongoIndexModel    mongo.IndexModel
	}
	mocks struct {
		dbCollection          *mockDbCollectionInterface
		dbCollectionIndexView *mockDbCollectionIndexViewInterface
		dbIterator            *mockDbIteratorInterface
	}
}

func TestDbIndexManagerService(t *testing.T) {
	test := dbIndexManagerServiceTest{}
	test.dbIndexManagerService = newDbIndexManagerService()
	test.prepareMocks()
	test.prepareDataForTest()

	t.Parallel()
	t.Run("testSuccessfulRetrievingOfCollectionIndexes", test.testSuccessfulRetrievingOfCollectionIndexes)
	t.Run("testSuccessfulRetrievingIndexesIterator", test.testSuccessfulRetrievingIndexesIterator)
	t.Run("testPanicOnRetrievingIndexesIterator", test.testPanicOnRetrievingIndexesIterator)
	t.Run("testSuccessfulMappingOfIndexesToStructSlice", test.testSuccessfulMappingOfIndexesToStructSlice)
	t.Run(
		"testPanicsOnMappingOfIndexesToStructSliceFailure",
		test.testPanicsOnMappingOfIndexesToStructSliceFailure,
	)
	t.Run("testSuccessfulIndexCreation", test.testSuccessfulIndexCreation)
	t.Run("testPanicsOnIndexCreationFailure", test.testPanicsOnIndexCreationFailure)
}

func (test *dbIndexManagerServiceTest) testSuccessfulRetrievingOfCollectionIndexes(t *testing.T) {
	test.mocks.dbCollection.
		On("Indexes").
		Return(test.dataForTest.mongoIndexView).
		Once()

	actualIndexView := test.dbIndexManagerService.retrieveCollectionIndexes(test.mocks.dbCollection)
	assert.Equal(t, test.dataForTest.mongoIndexView, actualIndexView)
}

func (test *dbIndexManagerServiceTest) testSuccessfulRetrievingIndexesIterator(t *testing.T) {
	test.mocks.dbCollectionIndexView.
		On("List", context.TODO()).
		Return(test.dataForTest.mongoCursor, nil).
		Once()

	actualDbIterator := test.dbIndexManagerService.indexesIterator(test.mocks.dbCollectionIndexView)
	assert.Equal(t, test.dataForTest.mongoCursor, actualDbIterator)
}

func (test *dbIndexManagerServiceTest) testPanicOnRetrievingIndexesIterator(t *testing.T) {
	test.mocks.dbCollectionIndexView.
		On("List", context.TODO()).
		Return(nil, test.dataForTest.expectedError).
		Once()

	assert.PanicsWithValue(t, test.dataForTest.expectedPanicValue, func() {
		actualDbIterator := test.dbIndexManagerService.indexesIterator(test.mocks.dbCollectionIndexView)
		assert.Nil(t, actualDbIterator)
	})
}

func (test *dbIndexManagerServiceTest) testSuccessfulMappingOfIndexesToStructSlice(t *testing.T) {
	dtoSlice := new([]struct {
		firstParameter  string
		secondParameter int
	})
	test.mocks.dbIterator.
		On("All", context.TODO(), dtoSlice).
		Return(nil).
		Once().
		Run(func(args mock.Arguments) {
			dtoSlice := args.Get(1).(*[]struct {
				firstParameter  string
				secondParameter int
			})
			*dtoSlice = test.dataForTest.expectedDtoSlice
		})

	test.dbIndexManagerService.mapIndexesToStructSlice(test.mocks.dbIterator, dtoSlice)
	assert.Equal(t, test.dataForTest.expectedDtoSlice, *dtoSlice)
}

func (test *dbIndexManagerServiceTest) testPanicsOnMappingOfIndexesToStructSliceFailure(t *testing.T) {
	dtoSlice := new([]struct {
		firstParameter  string
		secondParameter int
	})
	test.mocks.dbIterator.
		On("All", context.TODO(), dtoSlice).
		Return(test.dataForTest.expectedError).
		Once()

	assert.PanicsWithValue(t, test.dataForTest.expectedPanicValue, func() {
		test.dbIndexManagerService.mapIndexesToStructSlice(test.mocks.dbIterator, dtoSlice)
	})
}

func (test *dbIndexManagerServiceTest) testSuccessfulIndexCreation(_ *testing.T) {
	test.mocks.dbCollectionIndexView.
		On("CreateOne", context.TODO(), test.dataForTest.mongoIndexModel).
		Return("", nil).
		Once()

	test.dbIndexManagerService.createIndex(
		test.mocks.dbCollectionIndexView,
		test.dataForTest.mongoIndexModel,
	)
}

func (test *dbIndexManagerServiceTest) testPanicsOnIndexCreationFailure(t *testing.T) {
	test.mocks.dbCollectionIndexView.
		On("CreateOne", context.TODO(), test.dataForTest.mongoIndexModel).
		Return("", test.dataForTest.expectedError).
		Once()

	assert.PanicsWithValue(t, test.dataForTest.expectedPanicValue, func() {
		test.dbIndexManagerService.createIndex(
			test.mocks.dbCollectionIndexView,
			test.dataForTest.mongoIndexModel,
		)
	})
}

func (test *dbIndexManagerServiceTest) prepareMocks() {
	test.mocks.dbCollection = new(mockDbCollectionInterface)
	test.mocks.dbCollectionIndexView = new(mockDbCollectionIndexViewInterface)
	test.mocks.dbIterator = new(mockDbIteratorInterface)
}

func (test *dbIndexManagerServiceTest) prepareDataForTest() {
	test.dataForTest.mongoIndexView = mongo.IndexView{}
	test.dataForTest.mongoCursor = new(mongo.Cursor)
	test.dataForTest.expectedError = errors.New("some error occurred")
	test.dataForTest.expectedPanicValue = fmt.Sprint(test.dataForTest.expectedError.Error())
	dto := struct {
		firstParameter  string
		secondParameter int
	}{
		firstParameter:  "test string",
		secondParameter: 123,
	}
	test.dataForTest.expectedDtoSlice = []struct {
		firstParameter  string
		secondParameter int
	}{dto, dto, dto}
	test.dataForTest.mongoIndexModel = mongo.IndexModel{}
}

type getCollectionForDataServiceTest struct {
	getCollectionForDataService getCollectionForDataServiceInterface
	dataForTest                 struct {
		successfulExecutionDataTable []struct {
			data           interface{}
			collectionName string
		}
		mongoCollection *mongo.Collection
	}
	mocks struct {
		db *mockDbInterface
	}
}
type testStructForCollection struct{}
type someOtherCollectionTestStruct struct{}

func TestGetCollectionForDataService(t *testing.T) {
	test := getCollectionForDataServiceTest{}
	test.prepareDataForTest()
	test.prepareMocks()
	test.getCollectionForDataService = newGetCollectionForDataService(test.mocks.db)

	t.Parallel()
	t.Run("testSuccessfulExecution", test.testSuccessfulExecution)
}

func (test *getCollectionForDataServiceTest) testSuccessfulExecution(t *testing.T) {
	for _, tableRow := range test.dataForTest.successfulExecutionDataTable {
		test.mocks.db.
			On("Collection", tableRow.collectionName).
			Return(test.dataForTest.mongoCollection).
			Once()

		actualCollection := test.getCollectionForDataService.execute(tableRow.data)
		assert.Equal(t, test.dataForTest.mongoCollection, actualCollection)
	}
}

func (test *getCollectionForDataServiceTest) prepareDataForTest() {
	test.dataForTest.successfulExecutionDataTable = []struct {
		data           interface{}
		collectionName string
	}{
		{data: testStructForCollection{}, collectionName: "testStructForCollection"},
		{data: someOtherCollectionTestStruct{}, collectionName: "someOtherCollectionTestStruct"},
		{data: new(testStructForCollection), collectionName: "testStructForCollection"},
		{data: new(someOtherCollectionTestStruct), collectionName: "someOtherCollectionTestStruct"},
	}
	test.dataForTest.mongoCollection = new(mongo.Collection)
}

func (test *getCollectionForDataServiceTest) prepareMocks() {
	test.mocks.db = new(mockDbInterface)
}
