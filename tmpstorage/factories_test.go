package tmpstorage

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/mongo"
	"log"
	"reflect"
	"testing"
	"time"
)

type dataToStoreDtoFactoryTest struct {
	dataToStoreDtoFactory DataToStoreDtoFactoryInterface
	createNowTimeService  *mockCreateNowTimeServiceInterface
	dataForTest           struct {
		dataKey               string
		data                  testData
		time                  time.Time
		expirationDuration    time.Duration
		expiresAtFromDuration time.Time
	}
}

type testData struct {
	testSubData testSubData
	testInt     int
	testString  string
}

type testSubData struct {
	subTestInt int
}

func TestDataToStoreDtoFactory(t *testing.T) {
	factoryTest := dataToStoreDtoFactoryTest{}
	factoryTest.prepareDataForTest()
	factoryTest.createNowTimeService = &mockCreateNowTimeServiceInterface{}
	factoryTest.dataToStoreDtoFactory = newDataToStoreDtoFactory(factoryTest.createNowTimeService)

	t.Parallel()
	t.Run("testSuccessfulCreation", factoryTest.testSuccessfulCreation)
	t.Run("testSuccessfulCreationWithExistenceDuration", factoryTest.testSuccessfulCreationWithExistenceDuration)
}

func (test *dataToStoreDtoFactoryTest) testSuccessfulCreation(t *testing.T) {
	dataToStoreDto := test.dataToStoreDtoFactory.Create(
		test.dataForTest.dataKey,
		test.dataForTest.data,
		test.dataForTest.time,
	)

	assert.IsType(t, DataToStoreDto{}, dataToStoreDto)
	assert.Equal(t, test.dataForTest.dataKey, dataToStoreDto.DataKey)
	assert.Equal(t, test.dataForTest.data, dataToStoreDto.Data)
	assert.Equal(t, test.dataForTest.time, dataToStoreDto.ExpiresAt)
}

func (test *dataToStoreDtoFactoryTest) testSuccessfulCreationWithExistenceDuration(t *testing.T) {
	test.createNowTimeService.
		On("execute").
		Return(test.dataForTest.time).
		Once()

	dataToStoreDto := test.dataToStoreDtoFactory.CreateWithExistenceDuration(
		test.dataForTest.dataKey,
		test.dataForTest.data,
		test.dataForTest.expirationDuration,
	)

	assert.IsType(t, DataToStoreDto{}, dataToStoreDto)
	assert.Equal(t, test.dataForTest.dataKey, dataToStoreDto.DataKey)
	assert.Equal(t, test.dataForTest.data, dataToStoreDto.Data)
	assert.Equal(t, test.dataForTest.expiresAtFromDuration, dataToStoreDto.ExpiresAt)
}

func (test *dataToStoreDtoFactoryTest) prepareDataForTest() {
	test.dataForTest.dataKey = "testDataKey"
	test.dataForTest.data = testData{
		testSubData: testSubData{subTestInt: 777},
		testInt:     123,
		testString:  "test string",
	}
	timeForTest, err := time.Parse("2006-01-02 15:04:05", "2019-12-27 00:28:25")
	if err != nil {
		log.Panic(err)
	}
	test.dataForTest.time = timeForTest
	test.dataForTest.expirationDuration = time.Duration(10)
	test.dataForTest.expiresAtFromDuration = timeForTest.Add(test.dataForTest.expirationDuration)
}

type dbConnectorFactoryTest struct {
	dbConnectorFactory *dbConnectorFactory
	dataForTest        struct {
		mockMode               dbConnectorMode
		mockModeWithError      dbConnectorMode
		defaultMode            dbConnectorMode
		prodDbConnectorPointer uintptr
	}
}

func TestDbConnectorFactory(t *testing.T) {
	test := new(dbConnectorFactoryTest)
	test.prepareDataForTest()
	test.dbConnectorFactory = newDbConnectorFactory()

	t.Parallel()
	t.Run("testMockDbConnectorCreation", test.testMockDbConnectorCreation)
	t.Run("testMockDbConnectorWithErrorCreation", test.testMockDbConnectorWithErrorCreation)
	t.Run("testDefaultDbConnectorCreation", test.testDefaultDbConnectorCreation)
}

func (test *dbConnectorFactoryTest) testMockDbConnectorCreation(t *testing.T) {
	actualResult := test.dbConnectorFactory.create(test.dataForTest.mockMode)

	assert.NotNil(t, actualResult)
	assert.IsType(t, new(dbConnector), &actualResult)
	assert.NotEqual(t, test.dataForTest.prodDbConnectorPointer, reflect.ValueOf(actualResult).Pointer())
}

func (test *dbConnectorFactoryTest) testMockDbConnectorWithErrorCreation(t *testing.T) {
	actualResult := test.dbConnectorFactory.create(test.dataForTest.mockModeWithError)

	assert.NotNil(t, actualResult)
	assert.IsType(t, new(dbConnector), &actualResult)
	assert.NotEqual(t, test.dataForTest.prodDbConnectorPointer, reflect.ValueOf(actualResult).Pointer())
}

func (test *dbConnectorFactoryTest) testDefaultDbConnectorCreation(t *testing.T) {
	actualResult := test.dbConnectorFactory.create(test.dataForTest.defaultMode)

	assert.NotNil(t, actualResult)
	assert.Equal(t, test.dataForTest.prodDbConnectorPointer, reflect.ValueOf(actualResult).Pointer())
}

func (test *dbConnectorFactoryTest) prepareDataForTest() {
	test.dataForTest.defaultMode = ""
	test.dataForTest.mockMode = mockDbConnectorMode
	test.dataForTest.mockModeWithError = mockDbConnectorWithErrorMode
	test.dataForTest.prodDbConnectorPointer = reflect.ValueOf(mongo.Connect).Pointer()
}

type createIndexIfNotExistsServiceFactoryTest struct {
	createIndexIfNotExistsServiceFactory createIndexIfNotExistsServiceFactoryInterface
	mocks                                struct {
		getBsonParameterNameForDataToStoreDtoService *mockGetBsonParameterNameForDataToStoreDtoServiceInterface
		dbIndexManagerService                        *mockDbIndexManagerServiceInterface
		dataKeyDbIndexIsDtoIsValidSpecification      *mockDataKeyDbIndexIsDtoIsValidSpecificationInterface
		expiresAtDbIndexDtoIsValidSpecification      *mockExpiresAtDbIndexDtoIsValidSpecificationInterface
		logger                                       *mockLoggerServiceInterface
	}
	dataForTest struct {
		dataKeyParameterID   *dataToStoreParameterID
		expiresAtParameterID *dataToStoreParameterID
		dataParameterID      *dataToStoreParameterID
		expectedPanicValue   string
	}
}

func TestCreateIndexIfNotExistsServiceFactoryTest(t *testing.T) {
	test := &createIndexIfNotExistsServiceFactoryTest{}
	test.prepareMocks()
	test.prepareDataForTest()
	test.createIndexIfNotExistsServiceFactory = newCreateIndexIfNotExistsServiceFactory(
		test.mocks.getBsonParameterNameForDataToStoreDtoService,
		test.mocks.dbIndexManagerService,
		test.mocks.dataKeyDbIndexIsDtoIsValidSpecification,
		test.mocks.expiresAtDbIndexDtoIsValidSpecification,
		test.mocks.logger,
	)

	t.Parallel()
	t.Run("testCreateUniqueIndexForDataKeyServiceCreation", test.testCreateUniqueIndexForDataKeyServiceCreation)
	t.Run("testCreateTTLIndexForExpiresAtServiceCreation", test.testCreateTTLIndexForExpiresAtServiceCreation)
	t.Run("testPanicsOnNotSupportedParameterID", test.testPanicsOnNotSupportedParameterID)
}

func (test *createIndexIfNotExistsServiceFactoryTest) testCreateUniqueIndexForDataKeyServiceCreation(
	t *testing.T,
) {
	parameterID := test.dataForTest.dataKeyParameterID
	actualService := test.createIndexIfNotExistsServiceFactory.create(parameterID)

	assert.IsType(t, &createUniqueIndexForDataKeyInCollectionIfNotExistsService{}, actualService)
}

func (test *createIndexIfNotExistsServiceFactoryTest) testCreateTTLIndexForExpiresAtServiceCreation(
	t *testing.T,
) {
	parameterID := test.dataForTest.expiresAtParameterID
	actualService := test.createIndexIfNotExistsServiceFactory.create(parameterID)

	assert.IsType(t, &createTTLIndexForExpiresAtInCollectionIfNotExistsService{}, actualService)
}

func (test *createIndexIfNotExistsServiceFactoryTest) testPanicsOnNotSupportedParameterID(
	t *testing.T,
) {
	parameterID := test.dataForTest.dataParameterID
	assert.PanicsWithValue(t, test.dataForTest.expectedPanicValue, func() {
		actualService := test.createIndexIfNotExistsServiceFactory.create(parameterID)
		assert.Nil(t, actualService)
	})
}

func (test *createIndexIfNotExistsServiceFactoryTest) prepareMocks() {
	test.mocks.getBsonParameterNameForDataToStoreDtoService = new(
		mockGetBsonParameterNameForDataToStoreDtoServiceInterface,
	)
	test.mocks.dbIndexManagerService = new(mockDbIndexManagerServiceInterface)
	test.mocks.dataKeyDbIndexIsDtoIsValidSpecification = new(mockDataKeyDbIndexIsDtoIsValidSpecificationInterface)
	test.mocks.expiresAtDbIndexDtoIsValidSpecification = new(mockExpiresAtDbIndexDtoIsValidSpecificationInterface)
	test.mocks.logger = new(mockLoggerServiceInterface)
}

func (test *createIndexIfNotExistsServiceFactoryTest) prepareDataForTest() {
	test.dataForTest.dataKeyParameterID = newDataToStoreParameterIDWithPanicOnError(dataKeyParameterID)
	test.dataForTest.expiresAtParameterID = newDataToStoreParameterIDWithPanicOnError(expiresAtParameterID)
	test.dataForTest.dataParameterID = newDataToStoreParameterIDWithPanicOnError(dataParameterID)
	test.dataForTest.expectedPanicValue = fmt.Sprint(newParameterIDIsNotSupportedError(dataParameterID))
}
