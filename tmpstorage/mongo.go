package tmpstorage

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"reflect"
	"strings"
)

type dbCollectionInterface interface {
	InsertOne(
		ctx context.Context,
		document interface{},
		opts ...*options.InsertOneOptions,
	) (
		*mongo.InsertOneResult,
		error,
	)
	DeleteOne(ctx context.Context, filter interface{}, opts ...*options.DeleteOptions) (*mongo.DeleteResult, error)
	FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) *mongo.SingleResult
	UpdateOne(
		ctx context.Context,
		filter interface{},
		update interface{},
		opts ...*options.UpdateOptions,
	) (
		*mongo.UpdateResult,
		error,
	)
	Indexes() mongo.IndexView
	Name() string
}

type dbCollectionIndexViewInterface interface {
	List(ctx context.Context, opts ...*options.ListIndexesOptions) (*mongo.Cursor, error)
	CreateOne(ctx context.Context, model mongo.IndexModel, opts ...*options.CreateIndexesOptions) (string, error)
}

type dbIteratorInterface interface {
	All(ctx context.Context, results interface{}) error
}

type dbFindOneResultInterface interface {
	Decode(v interface{}) error
}

type dbInterface interface {
	Collection(name string, opts ...*options.CollectionOptions) *mongo.Collection
}

type dbConnectionInterface interface {
	Ping(ctx context.Context, rp *readpref.ReadPref) error
	Database(name string, opts ...*options.DatabaseOptions) *mongo.Database
}

//type connectToMongoDbServiceInterface interface {
//	connect() (dbConnectionInterface, error)
//	database(connection dbConnectionInterface) dbInterface
//}

type (
	dbURL  string
	dbName string
)

type connectToMongoDbService struct {
	dbConnector dbConnector
	dbURL       dbURL
	dbName      dbName
}

func newConnectToMongoDbService(
	dbConnector dbConnector,
	url dbURL,
	dbName dbName,
) *connectToMongoDbService {
	return &connectToMongoDbService{
		dbConnector: dbConnector,
		dbURL:       url,
		dbName:      dbName,
	}
}

func (service *connectToMongoDbService) connect() (dbConnectionInterface, error) {
	connectionOptions := options.Client().ApplyURI(string(service.dbURL))
	return service.dbConnector(context.TODO(), connectionOptions)
}

func (service *connectToMongoDbService) database(connection dbConnectionInterface) dbInterface {
	defer log.Println("Connected to storage database")
	return connection.Database(string(service.dbName))
}

type createIndexIfNotExistsServiceInterface interface {
	execute(collection dbCollectionInterface)
}

type createUniqueIndexForDataKeyInCollectionIfNotExistsService struct {
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface
	parameterID                                  *dataToStoreParameterID
	dbIndexManagerService                        dbIndexManagerServiceInterface
	dataKeyDbIndexIsDtoIsValidSpecification      dataKeyDbIndexIsDtoIsValidSpecificationInterface
	logger                                       loggerServiceInterface
}

func newCreateUniqueIndexForDataKeyInCollectionIfNotExistsService(
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface,
	parameterID *dataToStoreParameterID,
	dbIndexManagerService dbIndexManagerServiceInterface,
	dataKeyDbIndexIsDtoIsValidSpecification dataKeyDbIndexIsDtoIsValidSpecificationInterface,
	logger loggerServiceInterface,
) (
	*createUniqueIndexForDataKeyInCollectionIfNotExistsService,
	error,
) {
	service := &createUniqueIndexForDataKeyInCollectionIfNotExistsService{
		parameterID: parameterID,
		getBsonParameterNameForDataToStoreDtoService: getBsonParameterNameForDataToStoreDtoService,
		dbIndexManagerService:                        dbIndexManagerService,
		dataKeyDbIndexIsDtoIsValidSpecification:      dataKeyDbIndexIsDtoIsValidSpecification,
		logger:                                       logger,
	}

	err := service.assertIsDataKeyParameterID()
	if err != nil {
		return nil, err
	}

	return service, nil
}

func (service *createUniqueIndexForDataKeyInCollectionIfNotExistsService) execute(
	collection dbCollectionInterface,
) {
	indexView := service.dbIndexManagerService.retrieveCollectionIndexes(collection)
	indexesIterator := service.dbIndexManagerService.indexesIterator(indexView)

	var dataKeyDbIndexDtoSlice []dataKeyDbIndexDto
	service.dbIndexManagerService.mapIndexesToStructSlice(indexesIterator, &dataKeyDbIndexDtoSlice)

	for _, dto := range dataKeyDbIndexDtoSlice {
		if true == service.dataKeyDbIndexIsDtoIsValidSpecification.isSatisfiedBy(dto) {
			return
		}
	}

	dataKeyDbIndexField := service.getBsonParameterNameForDataToStoreDtoService.execute(service.parameterID)
	uniqueKeyIndexOptions := options.IndexOptions{}
	uniqueKeyIndexOptions.SetUnique(true)
	uniqueKeyIndex := mongo.IndexModel{
		Keys:    bson.M{dataKeyDbIndexField: 1},
		Options: &uniqueKeyIndexOptions,
	}

	service.dbIndexManagerService.createIndex(indexView, uniqueKeyIndex)

	service.logger.log(
		fmt.Sprintf(
			"Index for field '%s' in collection '%s' has been created",
			dataKeyDbIndexField,
			collection.Name(),
		),
		debugLevel,
	)
}

func (service *createUniqueIndexForDataKeyInCollectionIfNotExistsService) assertIsDataKeyParameterID() error {
	if dataKeyParameterID != service.parameterID.value {
		return newParameterIDIsNotSupportedError(service.parameterID.value)
	}

	return nil
}

const (
	indexOptionExpireAfterSecondsValue int32 = 0
)

type createTTLIndexForExpiresAtInCollectionIfNotExistsService struct {
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface
	parameterID                                  *dataToStoreParameterID
	dbIndexManagerService                        dbIndexManagerServiceInterface
	expiresAtDbIndexDtoIsValidSpecification      expiresAtDbIndexDtoIsValidSpecificationInterface
	logger                                       loggerServiceInterface
}

func newCreateTTLIndexForExpiresAtInCollectionIfNotExistsService(
	getBsonParameterNameForDataToStoreDtoService getBsonParameterNameForDataToStoreDtoServiceInterface,
	parameterID *dataToStoreParameterID,
	dbIndexManagerService dbIndexManagerServiceInterface,
	expiresAtDbIndexDtoIsValidSpecification expiresAtDbIndexDtoIsValidSpecificationInterface,
	logger loggerServiceInterface,
) (
	*createTTLIndexForExpiresAtInCollectionIfNotExistsService,
	error,
) {
	service := &createTTLIndexForExpiresAtInCollectionIfNotExistsService{
		getBsonParameterNameForDataToStoreDtoService: getBsonParameterNameForDataToStoreDtoService,
		parameterID:                             parameterID,
		dbIndexManagerService:                   dbIndexManagerService,
		expiresAtDbIndexDtoIsValidSpecification: expiresAtDbIndexDtoIsValidSpecification,
		logger:                                  logger,
	}

	err := service.assertIsExpiresAtParameterID()
	if err != nil {
		return nil, err
	}

	return service, nil
}

func (service *createTTLIndexForExpiresAtInCollectionIfNotExistsService) execute(
	collection dbCollectionInterface,
) {
	indexView := service.dbIndexManagerService.retrieveCollectionIndexes(collection)
	indexesIterator := service.dbIndexManagerService.indexesIterator(indexView)

	var expiresAtDbIndexDtoSlice []expiresAtDbIndexDto
	service.dbIndexManagerService.mapIndexesToStructSlice(indexesIterator, &expiresAtDbIndexDtoSlice)

	for _, dto := range expiresAtDbIndexDtoSlice {
		if true == service.expiresAtDbIndexDtoIsValidSpecification.isSatisfiedBy(dto) {
			return
		}
	}

	expiresAtDbIndexField := service.getBsonParameterNameForDataToStoreDtoService.execute(service.parameterID)
	expireAfterSecondsIndexOptions := options.IndexOptions{}
	expireAfterSecondsIndexOptions.SetExpireAfterSeconds(indexOptionExpireAfterSecondsValue)
	expireAfterSecondsIndex := mongo.IndexModel{
		Keys:    bson.M{expiresAtDbIndexField: 1},
		Options: &expireAfterSecondsIndexOptions,
	}

	service.dbIndexManagerService.createIndex(indexView, expireAfterSecondsIndex)

	service.logger.log(
		fmt.Sprintf(
			"Index for field '%s' in collection '%s' has been created",
			expiresAtDbIndexField,
			collection.Name(),
		),
		debugLevel,
	)
}

func (service *createTTLIndexForExpiresAtInCollectionIfNotExistsService) assertIsExpiresAtParameterID() error {
	if expiresAtParameterID != service.parameterID.value {
		return newParameterIDIsNotSupportedError(service.parameterID.value)
	}

	return nil
}

type dbIndexManagerServiceInterface interface {
	retrieveCollectionIndexes(collectionInterface dbCollectionInterface) dbCollectionIndexViewInterface
	indexesIterator(indexView dbCollectionIndexViewInterface) dbIteratorInterface
	//mapIndexesToStructSlice maps indexes to slice of structures.
	//expectedDtoSlicePointer must be a pointer to struct slice
	mapIndexesToStructSlice(indexesIterator dbIteratorInterface, dtoSlicePointer interface{})
	createIndex(indexView dbCollectionIndexViewInterface, model mongo.IndexModel)
}

type dbIndexManagerService struct {
}

func newDbIndexManagerService() *dbIndexManagerService {
	return &dbIndexManagerService{}
}

func (service *dbIndexManagerService) retrieveCollectionIndexes(
	collectionInterface dbCollectionInterface,
) dbCollectionIndexViewInterface {
	return collectionInterface.Indexes()
}

func (service *dbIndexManagerService) indexesIterator(
	indexView dbCollectionIndexViewInterface,
) dbIteratorInterface {
	iterator, err := indexView.List(context.TODO())
	if err != nil {
		log.Panic(err)
	}

	return iterator
}

//mapIndexesToStructSlice maps indexes to slice of structures.
//expectedDtoSlicePointer must be a pointer to struct slice
func (service *dbIndexManagerService) mapIndexesToStructSlice(
	indexesIterator dbIteratorInterface,
	dtoSlicePointer interface{},
) {
	err := indexesIterator.All(context.TODO(), dtoSlicePointer)
	if err != nil {
		log.Panic(err)
	}
}

func (service *dbIndexManagerService) createIndex(
	indexView dbCollectionIndexViewInterface,
	model mongo.IndexModel,
) {
	_, err := indexView.CreateOne(context.TODO(), model)
	if err != nil {
		log.Panic(err)
	}
}

type getCollectionForDataServiceInterface interface {
	execute(data interface{}) dbCollectionInterface
}

type getCollectionForDataService struct {
	db dbInterface
}

func newGetCollectionForDataService(db dbInterface) *getCollectionForDataService {
	return &getCollectionForDataService{db: db}
}

func (service *getCollectionForDataService) execute(data interface{}) dbCollectionInterface {
	dataTypeString := reflect.TypeOf(data).Name()

	if reflect.TypeOf(data).Kind() == reflect.Ptr {
		dataTypeString = reflect.Indirect(reflect.ValueOf(data)).Type().Name()
	}

	collectionName := strings.Replace(
		dataTypeString,
		string(dataTypeString[0]),
		strings.ToLower(string(dataTypeString[0])),
		1,
	)
	collection := service.db.Collection(collectionName)

	return collection
}
